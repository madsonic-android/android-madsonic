# Android-Madsonic

## About

Madsonic is a **music streaming app**, use it to connect to your own **Madsonic remote streaming server** and listen to your media wherever you go!

Media is cached for playback to save your mobile bandwidth and to make them available when you have no mobile connection!

It's a fork of the original Subsonic/DSub android client and adds some new enhancements.

## Feature

- Madsonic 6.3 support
- Madsonic Node support
- Google ChromeCast Support
- Android Wear Support
- Android Auto Support
- DLNA casting
- Replay Gain
- Better Offline mode
- Playlist Management
- Internet Radio tab
- Bookmarks tab
- Podcasts tab
- Broadcast tab
- Share tab
- Admin tab
- Chat tab
- and much more

In order to use all features in the client you must use your own Madsonic server.  http://madsonic.org


### License

Madsonic is open source software and licensed under the GNU General Public License version 3. 


> **Note:**
>
Fork of Dsub by Scott Jackson https://github.com/daneren2005/Subsonic/
>
Fork of Subsonic by Sindre Mehus https://sourceforge.net/p/subsonic/code/HEAD/tree/trunk/subsonic-android/
