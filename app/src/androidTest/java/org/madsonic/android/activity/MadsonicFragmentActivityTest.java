package org.madsonic.android.activity;

import org.madsonic.android.R;
import android.test.ActivityInstrumentationTestCase2;

public class MadsonicFragmentActivityTest extends
		ActivityInstrumentationTestCase2<MadsonicFragmentActivity> {

	private MadsonicFragmentActivity activity;

	public MadsonicFragmentActivityTest() {
		super(MadsonicFragmentActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	    activity = getActivity();
	}

	/**
	 * Test the main layout.
	 */
	public void testLayout() {
		assertNotNull(activity.findViewById(R.id.content_frame));
	}
	
	/**
	 * Test the bottom bar.
	 */
	public void testBottomBar() {
		assertNotNull(activity.findViewById(R.id.bottom_bar));
	}
}
