/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.widget.TextView;

import org.madsonic.android.R;

public class BasicHeaderView extends UpdateView<String> {
	TextView nameView;

	public BasicHeaderView(Context context) {
		this(context, R.layout.basic_header);
	}
	public BasicHeaderView(Context context, int layout) {
		super(context, false);

		LayoutInflater.from(context).inflate(layout, this, true);
		nameView = (TextView) findViewById(R.id.item_name);
		Typeface typeface = ResourcesCompat.getFont(context, R.font.dosis_500);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			nameView.setTypeface(typeface);
		}
	}

	protected void setObjectImpl(String string) {
		nameView.setText(string);
	}
}
