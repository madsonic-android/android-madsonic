/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import org.madsonic.android.R;
import org.madsonic.android.util.Constants;

/**
 * SeekBar preference to set the shake force threshold.
 */
public class SeekBarPreference extends DialogPreference implements SeekBar.OnSeekBarChangeListener {
	private static final String TAG = SeekBarPreference.class.getSimpleName();
	/**
	 * The current value.
	 */
	private String mValue;
	private int mMin;
	private int mMax;
	private float mStepSize;
	private String mDisplay;

	/**
	 * Our context (needed for getResources())
	 */
	private Context mContext;

	/**
	 * TextView to display current threshold.
	 */
	private TextView mValueText;

	public SeekBarPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		mContext = context;

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SeekBarPreference);
		mMin = a.getInteger(R.styleable.SeekBarPreference_min, 0);
		mMax = a.getInteger(R.styleable.SeekBarPreference_max, 100);
		mStepSize = a.getFloat(R.styleable.SeekBarPreference_stepSize, 1f);
		mDisplay = a.getString(R.styleable.SeekBarPreference_display);
		if(mDisplay == null) {
			mDisplay = "%.0f";
		}
	}

	@Override
	public CharSequence getSummary()
	{
		return getSummary(mValue);
	}

	@Override
	protected Object onGetDefaultValue(TypedArray a, int index)
	{
		return a.getString(index);
	}

	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue)
	{
		mValue = restoreValue ? getPersistedString((String) defaultValue) : (String)defaultValue;
	}

	/**
	 * Create the summary for the given value.
	 *
	 * @param value The force threshold.
	 * @return A string representation of the threshold.
	 */
	private String getSummary(String value) {
		try {
			int val = Integer.parseInt(value);
			return String.format(mDisplay, (val + mMin) / mStepSize);
		} catch (Exception e) {
			return "";
		}
	}

	@Override
	protected View onCreateDialogView()
	{
		View view = super.onCreateDialogView();

		mValueText = (TextView)view.findViewById(R.id.value);
		mValueText.setText(getSummary(mValue));

		SeekBar seekBar = (SeekBar)view.findViewById(R.id.seek_bar);
		seekBar.setMax(mMax - mMin);
		try {
			seekBar.setProgress(Integer.parseInt(mValue));
		} catch(Exception e) {
			seekBar.setProgress(0);
		}
		seekBar.setOnSeekBarChangeListener(this);

		return view;
	}

	@Override
	protected void onDialogClosed(boolean positiveResult)
	{
		if(positiveResult) {
			persistString(mValue);
			notifyChanged();
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{
		if (fromUser) {
			mValue = String.valueOf(progress);
			mValueText.setText(getSummary(mValue));
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{
	}
}
