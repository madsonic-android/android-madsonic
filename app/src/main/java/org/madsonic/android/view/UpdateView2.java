/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.widget.ImageView;

public abstract class UpdateView2<T1, T2> extends UpdateView<T1> {
	protected T2 item2;

	public UpdateView2(Context context) {
		super(context);
	}

	public UpdateView2(Context context, boolean autoUpdate) {
		super(context, autoUpdate);
	}

	public final void setObject(T1 obj1) {
		setObject(obj1, null);
	}
	@Override
	public void setObject(T1 obj1, Object obj2) {
		if(item == obj1 && item2 == obj2) {
			return;
		}

		item = obj1;
		item2 = (T2) obj2;
		if(imageTask != null) {
			imageTask.cancel();
			imageTask = null;
		}
		if(coverArtView != null && coverArtView instanceof ImageView) {
			((ImageView) coverArtView).setImageDrawable(null);
		}

		setObjectImpl(item, item2);
		backgroundHandler.post(new Runnable() {
			@Override
			public void run() {
				updateBackground();
				uiHandler.post(new Runnable() {
					@Override
					public void run() {
						update();
					}
				});
			}
		});
	}

	protected final void setObjectImpl(T1 obj1) {
		setObjectImpl(obj1, null);
	}

	protected abstract void setObjectImpl(T1 obj1, T2 obj2);
}
