/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import org.madsonic.android.R;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.service.MusicServiceFactory;
import org.madsonic.android.util.Constants;
import org.madsonic.android.util.DrawableTint;
import org.madsonic.android.util.FileUtil;
import org.madsonic.android.util.Util;

public class AlbumListCountView extends UpdateView2<Integer, Void> {
	private final String TAG = AlbumListCountView.class.getSimpleName();

	private TextView titleView;
	private TextView countView;
	private int startCount;
	private int count = 0;

	public AlbumListCountView(Context context) {
		super(context, false);
		this.context = context;
		LayoutInflater.from(context).inflate(R.layout.basic_count_item, this, true);

		titleView = (TextView) findViewById(R.id.basic_count_name);
		countView = (TextView) findViewById(R.id.basic_count_count);

		Typeface typeface = ResourcesCompat.getFont(context, R.font.dosis_500);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			titleView.setTypeface(typeface);
			countView.setTypeface(typeface);
		}
	}

	protected void setObjectImpl(Integer albumListString, Void dummy) {
		titleView.setText(albumListString);

		SharedPreferences prefs = Util.getPreferences(context);
		startCount = prefs.getInt(Constants.PREFERENCES_KEY_RECENT_COUNT + Util.getActiveServer(context), 0);
		count = startCount;
		update();
	}

	@Override
	protected void updateBackground() {
		try {
			String recentAddedFile = Util.getCacheName(context, "recent_count");
			ArrayList<String> recents = FileUtil.deserialize(context, recentAddedFile, ArrayList.class);
			if (recents == null) {
				recents = new ArrayList<String>();
			}

			MusicService musicService = MusicServiceFactory.getMusicService(context);
			MusicDirectory recentlyAdded = musicService.getAlbumList("newest", 20, 0, false, context, null);

			// If first run, just put everything in it and return 0
			boolean firstRun = recents.isEmpty();

			// Count how many new albums are in the list
			count = 0;
			for (MusicDirectory.Entry album : recentlyAdded.getChildren()) {
				if (!recents.contains(album.getId())) {
					recents.add(album.getId());
					count++;
				}
			}

			// Keep recents list from growing infinitely
			while (recents.size() > 40) {
				recents.remove(0);
			}
			FileUtil.serialize(context, recents, recentAddedFile);

			if (!firstRun) {
				// Add the old count which will get cleared out after viewing recents
				count += startCount;
				SharedPreferences.Editor editor = Util.getPreferences(context).edit();
				editor.putInt(Constants.PREFERENCES_KEY_RECENT_COUNT + Util.getActiveServer(context), count);
				editor.commit();
			}
		} catch(Exception e) {
			Log.w(TAG, "Failed to refresh most recent count", e);
		}
	}

	@Override
	protected void update() {
		// Update count display with appropriate information
		if(count <= 0) {
			countView.setVisibility(View.GONE);
		} else {
			String displayName;
			if(count < 10) {
				displayName = "0" + count;
			} else {
				displayName = "" + count;
			}

			countView.setBackground(DrawableTint.getTintedDrawable(context, R.drawable.ic_number_border, R.attr.colorAccent));
			countView.setText(displayName);
			countView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick() {
		SharedPreferences.Editor editor = Util.getPreferences(context).edit();
		editor.putInt(Constants.PREFERENCES_KEY_RECENT_COUNT + Util.getActiveServer(context), 0);
		editor.commit();

		count = 0;
		update();
	}
}
