/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.madsonic.android.R;
import org.madsonic.android.domain.Genre;
import org.madsonic.android.domain.Mood;

public class MoodSongView extends UpdateView<Mood> {
	private static final String TAG = MoodSongView.class.getSimpleName();

	private TextView titleView;
	private TextView songsView;

	public MoodSongView(Context context) {
		super(context, false);
		LayoutInflater.from(context).inflate(R.layout.moods_list_song_item, this, true);

		titleView = (TextView) findViewById(R.id.moods_name);
		songsView = (TextView) findViewById(R.id.moods_songs);

		Typeface typeface = ResourcesCompat.getFont(context, R.font.dosis_400);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			titleView.setTypeface(typeface);
			songsView.setTypeface(typeface);
		}
	}

	public void setObjectImpl(Mood mood) {
		titleView.setText(mood.getName());

		 if(mood.getSongCount() != null) {
            songsView.setVisibility(View.VISIBLE);
            songsView.setText(context.getResources().getString(R.string.select_genre_songs, mood.getSongCount()));

		} else {
			songsView.setVisibility(View.GONE);
		}
	}
}
