/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Layout;
import android.text.style.LeadingMarginSpan;

/**
 * Created by Scott on 1/13/2015.
 */
public class MyLeadingMarginSpan2 implements LeadingMarginSpan.LeadingMarginSpan2 {
	private int margin;
	private int lines;

	public MyLeadingMarginSpan2(int lines, int margin) {
		this.margin = margin;
		this.lines = lines;
	}

	@Override
	public int getLeadingMargin(boolean first) {
		return first ? margin : 10;
	}

	@Override
	public int getLeadingMarginLineCount() {
		return lines;
	}

	@Override
	public void drawLeadingMargin(Canvas c, Paint p, int x, int dir,
								  int top, int baseline, int bottom, CharSequence text,
								  int start, int end, boolean first, Layout layout) {}
}
