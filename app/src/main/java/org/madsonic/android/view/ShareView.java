/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import org.madsonic.android.R;
import org.madsonic.android.domain.Share;

public class ShareView extends UpdateView<Share> {
	private static final String TAG = ShareView.class.getSimpleName();

	private TextView titleView;
	private TextView descriptionView;

	public ShareView(Context context) {
		super(context, false);
		LayoutInflater.from(context).inflate(R.layout.complex_list_item, this, true);

		titleView = (TextView) findViewById(R.id.item_name);
		descriptionView = (TextView) findViewById(R.id.item_description);
		starButton = (ImageButton) findViewById(R.id.item_star);
		starButton.setFocusable(false);
		moreButton = (ImageView) findViewById(R.id.item_more);
		moreButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				v.showContextMenu();
			}
		});
	}

	public void setObjectImpl(Share share) {
		titleView.setText(share.getName());
		if(share.getExpires() != null) {
			descriptionView.setText(context.getResources().getString(R.string.share_expires, new SimpleDateFormat("E MMM d, yyyy", Locale.ENGLISH).format(share.getExpires())));
		} else {
			descriptionView.setText(context.getResources().getString(R.string.share_expires_never));
		}
	}
}
