/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.domain.Playlist;
import org.madsonic.android.util.FileUtil;
import org.madsonic.android.util.SyncUtil;
import org.madsonic.android.util.Util;

public class PlaylistSongView extends UpdateView2<Playlist, List<MusicDirectory.Entry>> {
	private static final String TAG = PlaylistSongView.class.getSimpleName();

	private TextView titleView;
	private TextView countView;
	private int count = 0;

	public PlaylistSongView(Context context) {
		super(context, false);
		this.context = context;
		LayoutInflater.from(context).inflate(R.layout.basic_count_item, this, true);

		titleView = (TextView) findViewById(R.id.basic_count_name);
		countView = (TextView) findViewById(R.id.basic_count_count);
	}

	protected void setObjectImpl(Playlist playlist, List<MusicDirectory.Entry> songs) {
		count = 0;
		titleView.setText(playlist.getName());
		// Make sure to hide initially so it's not present briefly before update
		countView.setVisibility(View.GONE);
	}

	@Override
	protected void updateBackground() {
		// Make sure to reset when starting count
		count = 0;
		
		// Don't try to lookup playlist for Create New
		if(!"-1".equals(item.getId())) {
			MusicDirectory cache = FileUtil.deserialize(context, Util.getCacheName(context, "playlist", item.getId()), MusicDirectory.class);
			if(cache != null) {
				// Try to find song instances in the given playlists
				for(MusicDirectory.Entry song: item2) {
					if(cache.getChildren().contains(song)) {
						count++;
					}
				}
			}
		}
	}

	@Override
	protected void update() {
		// Update count display with appropriate information
		if(count <= 0) {
			countView.setVisibility(View.GONE);
		} else {
			String displayName;
			if(count < 10) {
				displayName = "0" + count;
			} else {
				displayName = "" + count;
			}

			countView.setText(displayName);
			countView.setVisibility(View.VISIBLE);
		}
	}
}
