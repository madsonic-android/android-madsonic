/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.madsonic.android.R;
import org.madsonic.android.domain.User;
import org.madsonic.android.util.ImageLoader;

public class UserView extends UpdateView2<User, ImageLoader> {
	private TextView usernameView;
	private ImageView avatarView;

	public UserView(Context context) {
		super(context, false);
		LayoutInflater.from(context).inflate(R.layout.user_list_item, this, true);

		usernameView = (TextView) findViewById(R.id.item_name);
		avatarView = (ImageView) findViewById(R.id.item_avatar);
		moreButton = (ImageView) findViewById(R.id.item_more);
		moreButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				v.showContextMenu();
			}
		});
	}

	protected void setObjectImpl(User user, ImageLoader imageLoader) {
		usernameView.setText(user.getUsername());
		imageTask = imageLoader.loadAvatar(context, avatarView, user.getUsername());
	}

	public void onUpdateImageView() {
		imageTask = item2.loadAvatar(context, avatarView, item.getUsername());
	}
}
