package org.madsonic.android.view;

public final class ViewType {

    public static final int VIEW_TYPE_HEADER           = 0;
    public static final int VIEW_TYPE_LINE             = 1;
    public static final int VIEW_TYPE_ALBUM_CELL       = 2;
    public static final int VIEW_TYPE_ALBUM_LINE       = 3;
    public static final int VIEW_TYPE_ALBUM_LIST       = 4;
    public static final int VIEW_TYPE_ALBUM_COUNT_LIST = 5;
    public static final int VIEW_TYPE_ARTIST           = 6;
    public static final int VIEW_TYPE_SONG             = 7;
    public static final int VIEW_TYPE_GENRE            = 8;
    public static final int VIEW_TYPE_MOOD             = 9;
    public static final int VIEW_TYPE_LOADING          = 10;
    public static final int VIEW_TYPE_PLAYLIST         = 11;
    public static final int VIEW_TYPE_BROADCAST        = 12;
    public static final int VIEW_TYPE_DOWNLOAD_FILE    = 13;
    public static final int VIEW_TYPE_INTERNET_RADIO   = 14;
    public static final int VIEW_TYPE_PODCAST_LEGACY   = 15;
    public static final int VIEW_TYPE_PODCAST_LINE     = 16;
    public static final int VIEW_TYPE_PODCAST_CELL     = 17;
    public static final int VIEW_TYPE_PODCAST_EPISODE  = 18;
    public static final int VIEW_TYPE_SETTING          = 19;
    public static final int VIEW_TYPE_SETTING_HEADER   = 20;
    public static final int VIEW_TYPE_SHARE            = 21;
    public static final int VIEW_TYPE_USER             = 22;
}
