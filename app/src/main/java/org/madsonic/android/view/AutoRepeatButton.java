/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

public class AutoRepeatButton extends ImageButton {

	private static final long initialRepeatDelay = 1000;
	private static final long repeatIntervalInMilliseconds = 300;
	private boolean doClick = true;
	private Runnable repeatEvent = null;

	private Runnable repeatClickWhileButtonHeldRunnable = new Runnable() {
		@Override
		public void run() {
			doClick = false;
			//Perform the present repetition of the click action provided by the user
			// in setOnClickListener().
			if(repeatEvent != null)
				repeatEvent.run();

			//Schedule the next repetitions of the click action, using a faster repeat
			// interval than the initial repeat delay interval.
			postDelayed(repeatClickWhileButtonHeldRunnable, repeatIntervalInMilliseconds);
		}
	};

	private void commonConstructorCode() {
		this.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction(); 
				if(action == MotionEvent.ACTION_DOWN) 
				{
					doClick = true;
					//Just to be sure that we removed all callbacks, 
					// which should have occurred in the ACTION_UP
					removeCallbacks(repeatClickWhileButtonHeldRunnable);

					//Schedule the start of repetitions after a one half second delay.
					postDelayed(repeatClickWhileButtonHeldRunnable, initialRepeatDelay);
					
					setPressed(true);
				}
				else if(action == MotionEvent.ACTION_UP) {
					//Cancel any repetition in progress.
					removeCallbacks(repeatClickWhileButtonHeldRunnable);

					if(doClick || repeatEvent == null) {
						performClick();
					}
					
					setPressed(false);
				}

				//Returning true here prevents performClick() from getting called 
				// in the usual manner, which would be redundant, given that we are 
				// already calling it above.
				return true;
			}
		});
	}
	
	public void setOnRepeatListener(Runnable runnable) {
		repeatEvent = runnable;
	}

	public AutoRepeatButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		commonConstructorCode();
	}


	public AutoRepeatButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonConstructorCode();
	}

	public AutoRepeatButton(Context context) {
		super(context);
		commonConstructorCode();
	}
}
