package org.madsonic.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class StartActivity extends AppCompatActivity {

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences settings = getSharedPreferences("prefs", 0);
        boolean firstRun = settings.getBoolean("firstRun", false);
        if (firstRun == false) {
            //if running for first time
            //Splash will load for first time
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("firstRun", true);
            editor.commit();
            Intent i = new Intent(StartActivity.this, SplashActivity.class);
            startActivity(i);
            finish();
        } else {
            Intent a = new Intent(StartActivity.this, MadsonicFragmentActivity.class);
            startActivity(a);  // Launch next activity
            finish();
        }
    }

}