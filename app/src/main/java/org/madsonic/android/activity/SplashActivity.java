package org.madsonic.android.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

import org.madsonic.android.R;

public class SplashActivity extends AppCompatActivity {

    /**
     * The thread to process splash screen events
     */
    private Thread mSplashThread;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Splash screen view
        setContentView(R.layout.spinner);

        final TextView brand = (TextView) findViewById(R.id.brand);
        Typeface typeface = Typeface.createFromAsset(this.getApplicationContext().getAssets(),"fonts/nisesonic.ttf");
        brand.setTypeface(typeface);

        // Start animating the image
        final ImageView splashImageView = (ImageView) findViewById(R.id.spin);
        splashImageView.setBackgroundResource(R.drawable.spinner_animation);
        final AnimationDrawable frameAnimation = (AnimationDrawable) splashImageView.getBackground();
        splashImageView.post(new Runnable() {
            @Override
            public void run() {
                frameAnimation.start();
            }
        });

        final SplashActivity splashActivity = this;

        // The thread to wait for splash screen events
        mSplashThread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        // Wait given period of time or exit on touch
                        wait(5000);
                    }
                } catch (InterruptedException ex) {
                }
                // Run next activity
                Intent intent = new Intent();
                intent.setClass(splashActivity, MadsonicFragmentActivity.class);
                startActivity(intent);
                frameAnimation.stop();
                finish();
            }
        };

        mSplashThread.start();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return false;
    }

    /**
     * Processes splash screen touch events
     */
    @Override
    public boolean onTouchEvent(MotionEvent evt) {
        if (evt.getAction() == MotionEvent.ACTION_DOWN) {
            synchronized (mSplashThread) {
                mSplashThread.notifyAll();
            }
        }
        return true;
    }

}