package org.madsonic.android.application;

import android.app.Application;

import org.madsonic.android.util.FontsOverride;

public class MadsonicApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FontsOverride.setDefaultFont(this, "DEFAULT", "dosis_400.ttf");

        //TooLargeTool.startLogging(this);

//        Bridge.initialize(getApplicationContext(), new SavedStateHandler() {
//            @Override
//            public void saveInstanceState(@NonNull Object target, @NonNull Bundle state) {
//                Icepick.saveInstanceState(target, state);
//            }
//
//            @Override
//            public void restoreInstanceState(@NonNull Object target, @Nullable Bundle state) {
//                Icepick.restoreInstanceState(target, state);
//            }
//        });
    }
}