/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;

import java.util.List;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import org.madsonic.android.R;
import org.madsonic.android.domain.Bookmark;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.util.Util;
import org.madsonic.android.view.SongView;
import org.madsonic.android.view.UpdateView;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_SONG;

public class BookmarkAdapter extends SectionAdapter<MusicDirectory.Entry> {
	private final static String TAG = BookmarkAdapter.class.getSimpleName();
	
	public BookmarkAdapter(Context activity, List<MusicDirectory.Entry> bookmarks, OnItemClickedListener listener) {
		super(activity, bookmarks);
		this.onItemClickedListener = listener;
		checkable = true;
	}

	@Override
	public UpdateView.UpdateViewHolder onCreateSectionViewHolder(ViewGroup parent, int viewType) {
		return new UpdateView.UpdateViewHolder(new SongView(context));
	}

	@Override
	public void onBindViewHolder(UpdateView.UpdateViewHolder holder, MusicDirectory.Entry item, int viewType) {
		SongView songView = (SongView) holder.getUpdateView();
		Bookmark bookmark = item.getBookmark();

		songView.setObject(item, true);

		// Add current position to duration
		TextView durationTextView = (TextView) songView.findViewById(R.id.song_duration);
		String duration = durationTextView.getText().toString();
		durationTextView.setText(Util.formatDuration(bookmark.getPosition() / 1000) + " / " + duration);
	}

	@Override
	public int getItemViewType(MusicDirectory.Entry item) {
		return VIEW_TYPE_SONG;
	}

	@Override
	public void onCreateActionModeMenu(Menu menu, MenuInflater menuInflater) {
		if(Util.isOffline(context)) {
			menuInflater.inflate(R.menu.multiselect_media_offline, menu);
		} else {
			menuInflater.inflate(R.menu.multiselect_media, menu);
		}

		menu.removeItem(R.id.menu_remove_playlist);
	}
}
