/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.domain.MusicDirectory.Entry;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.util.Util;
import org.madsonic.android.view.AlbumView;
import org.madsonic.android.view.BroadcastView;
import org.madsonic.android.view.SongView;
import org.madsonic.android.view.UpdateView;
import org.madsonic.android.view.UpdateView.UpdateViewHolder;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_ALBUM_CELL;
import static org.madsonic.android.view.ViewType.VIEW_TYPE_ALBUM_LINE;
import static org.madsonic.android.view.ViewType.VIEW_TYPE_BROADCAST;
import static org.madsonic.android.view.ViewType.VIEW_TYPE_SONG;

public class EntryGridAdapter extends SectionAdapter<Entry> {
	private static String TAG = EntryGridAdapter.class.getSimpleName();


	private ImageLoader imageLoader;
	private boolean largeAlbums;
	private boolean showArtist = false;
	private boolean showAlbum = false;
	private boolean removeFromPlaylist = false;
	private View header;

	public EntryGridAdapter(Context context, List<Entry> entries, ImageLoader imageLoader, boolean largeCell) {
		super(context, entries);
		this.imageLoader = imageLoader;
		this.largeAlbums = largeCell;

		// Always show artist if they aren't all the same
		String artist = null;
		for(MusicDirectory.Entry entry: entries) {
			if(artist == null) {
				artist = entry.getArtist();
			}

			if(artist != null && !artist.equals(entry.getArtist())) {
				showArtist = true;
			}
		}
		checkable = true;
	}

	@Override
	public UpdateViewHolder onCreateSectionViewHolder(ViewGroup parent, int viewType) {
		UpdateView updateView = null;
		if(viewType == VIEW_TYPE_ALBUM_LINE || viewType == VIEW_TYPE_ALBUM_CELL) {
			updateView = new AlbumView(context, viewType == VIEW_TYPE_ALBUM_CELL);

		} else if(viewType == VIEW_TYPE_SONG) {
			updateView = new SongView(context);

        } else if(viewType == VIEW_TYPE_BROADCAST) {
            updateView = new BroadcastView(context);
        }

		return new UpdateViewHolder(updateView);
	}

	@Override
	public void onBindViewHolder(UpdateViewHolder holder, Entry entry, int viewType) {
		UpdateView view = holder.getUpdateView();

		if(viewType == VIEW_TYPE_ALBUM_CELL || viewType == VIEW_TYPE_ALBUM_LINE) {
			AlbumView albumView = (AlbumView) view;
			albumView.setShowArtist(showArtist);
			albumView.setObject(entry, imageLoader);

		} else if(viewType == VIEW_TYPE_SONG) {
			SongView songView = (SongView) view;
			songView.setShowAlbum(showAlbum);
			songView.setObject(entry, checkable && !entry.isVideo());

        } else if(viewType == VIEW_TYPE_BROADCAST) {
            BroadcastView broadcastView = (BroadcastView) view;
            broadcastView.setObject(entry, imageLoader);
        }
	}

	public UpdateViewHolder onCreateHeaderHolder(ViewGroup parent) {
		return new UpdateViewHolder(header, false);
	}
	public void onBindHeaderHolder(UpdateViewHolder holder, String header, int sectionIndex) {

	}

	@Override
	public int getItemViewType(Entry entry) {
		if(entry.isDirectory()) {
			if (largeAlbums) {
				return VIEW_TYPE_ALBUM_CELL;
			} else {
				return VIEW_TYPE_ALBUM_LINE;
			}
		} else {
            if (entry.getUsername() != null) {
                return VIEW_TYPE_BROADCAST;
            } else {
                return VIEW_TYPE_SONG;
            }
		}
	}

	public void setHeader(View header) {
		this.header = header;
		this.singleSectionHeader = true;
	}
	public View getHeader() {
		return header;
	}

	public void setShowArtist(boolean showArtist) {
		this.showArtist = showArtist;
	}

	public void setShowAlbum(boolean showAlbum) {
		this.showAlbum = showAlbum;
	}

	public void removeAt(int index) {
		sections.get(0).remove(index);
		if(header != null) {
			index++;
		}
		notifyItemRemoved(index);
	}

	public void setRemoveFromPlaylist(boolean removeFromPlaylist) {
		this.removeFromPlaylist = removeFromPlaylist;
	}

	@Override
	public void onCreateActionModeMenu(Menu menu, MenuInflater menuInflater) {
		if(Util.isOffline(context)) {
			menuInflater.inflate(R.menu.multiselect_media_offline, menu);
		} else {
			menuInflater.inflate(R.menu.multiselect_media, menu);
		}

		if(!removeFromPlaylist) {
			menu.removeItem(R.id.menu_remove_playlist);
		}

		if(!selected.isEmpty()) {
			MenuItem starItem = menu.findItem(R.id.menu_star);
			if(starItem != null) {
				boolean isStarred = selected.get(0).isStarred();
				starItem.setTitle(isStarred ? R.string.common_unstar : R.string.common_star);
			}
		}
	}
}
