/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.domain.User;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.view.UpdateView;
import org.madsonic.android.view.UserView;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_USER;

public class UserAdapter extends SectionAdapter<User> {
/*	public static int VIEW_TYPE_USER = 1;*/

	private final ImageLoader imageLoader;

	public UserAdapter(Context context, List<User> users, ImageLoader imageLoader, OnItemClickedListener listener) {
		super(context, users);
		this.imageLoader = imageLoader;
		this.onItemClickedListener = listener;
	}

	@Override
	public UpdateView.UpdateViewHolder onCreateSectionViewHolder(ViewGroup parent, int viewType) {
		return new UpdateView.UpdateViewHolder(new UserView(context));
	}

	@Override
	public void onBindViewHolder(UpdateView.UpdateViewHolder holder, User item, int viewType) {
		holder.getUpdateView().setObject(item, imageLoader);
	}

	@Override
	public int getItemViewType(User item) {
		return VIEW_TYPE_USER;
	}
}