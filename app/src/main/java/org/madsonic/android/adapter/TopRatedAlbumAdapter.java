/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;

import java.util.List;

import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.view.FastScroller;

public class TopRatedAlbumAdapter extends EntryInfiniteGridAdapter implements FastScroller.BubbleTextGetter {
	public TopRatedAlbumAdapter(Context context, List<MusicDirectory.Entry> entries, ImageLoader imageLoader, boolean largeCell) {
		super(context, entries, imageLoader, largeCell);
	}

	@Override
	public String getTextToShowInBubble(int position) {
		// Make sure that we are not trying to get an item for the loading placeholder
		if(position >= sections.get(0).size()) {
			if(sections.get(0).size() > 0) {
				return getTextToShowInBubble(position - 1);
			} else {
				return "*";
			}
		} else {
			return Integer.toString(getItemForPosition(position).getRating());
		}
	}
}
