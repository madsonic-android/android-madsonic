/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;
import android.view.ViewGroup;

import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.view.BroadcastView;
import org.madsonic.android.view.SongView;
import org.madsonic.android.view.UpdateView;

import java.util.List;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_BROADCAST;

public class BroadcastAdapter extends SectionAdapter<MusicDirectory.Entry> {

    private ImageLoader imageLoader;

    public BroadcastAdapter(Context context, List<MusicDirectory.Entry> songs, ImageLoader imageLoader) {
		super(context, songs);
        this.imageLoader = imageLoader;
	}

	@Override
	public UpdateView.UpdateViewHolder onCreateSectionViewHolder(ViewGroup parent, int viewType) {
		return new UpdateView.UpdateViewHolder(new BroadcastView(context));
	}

    @Override
    public void onBindViewHolder(UpdateView.UpdateViewHolder holder, MusicDirectory.Entry item, int viewType) {
        BroadcastView broadcastView = (BroadcastView) holder.getUpdateView();
//        String username = item.getUsername();
//        ImageView avatar = (ImageView) songView.findViewById(R.id.nowplaying_avatar);
//        if (username != null) {
//            avatar.setImageDrawable(DrawableTint.getTintedDrawable(context, R.drawable.ic_action_bookmark_dark));
//            avatar.setVisibility(ViewGroup.VISIBLE);
//        } else {
//            avatar.setVisibility(ViewGroup.GONE);
//        }
        broadcastView.setObject(item, imageLoader);
    }

	@Override
	public int getItemViewType(MusicDirectory.Entry item) {
		return VIEW_TYPE_BROADCAST;
	}
}
