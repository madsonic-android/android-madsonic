/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;

import java.util.List;

import android.view.ViewGroup;
import org.madsonic.android.domain.Playlist;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.view.FastScroller;
import org.madsonic.android.view.PlaylistView;
import org.madsonic.android.view.UpdateView;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_PLAYLIST;

public class PlaylistAdapter extends SectionAdapter<Playlist> implements FastScroller.BubbleTextGetter {

	private ImageLoader imageLoader;
	private boolean largeCell;

	public PlaylistAdapter(Context context, List<Playlist> playlists, ImageLoader imageLoader, boolean largeCell, OnItemClickedListener listener) {
		super(context, playlists);
		this.imageLoader = imageLoader;
		this.largeCell = largeCell;
		this.onItemClickedListener = listener;
	}
	public PlaylistAdapter(Context context, List<String> headers, List<List<Playlist>> sections, ImageLoader imageLoader, boolean largeCell, OnItemClickedListener listener) {
		super(context, headers, sections);
		this.imageLoader = imageLoader;
		this.largeCell = largeCell;
		this.onItemClickedListener = listener;
	}

	@Override
	public UpdateView.UpdateViewHolder onCreateSectionViewHolder(ViewGroup parent, int viewType) {
		return new UpdateView.UpdateViewHolder(new PlaylistView(context, imageLoader, largeCell));
	}

	@Override
	public void onBindViewHolder(UpdateView.UpdateViewHolder holder, Playlist playlist, int viewType) {
		holder.getUpdateView().setObject(playlist);
		holder.setItem(playlist);
	}

	@Override
	public int getItemViewType(Playlist playlist) {
		return VIEW_TYPE_PLAYLIST;
	}

	@Override
	public String getTextToShowInBubble(int position) {
		Object item = getItemForPosition(position);
		if(item instanceof Playlist) {
			return getNameIndex(((Playlist) item).getName());
		} else {
			return null;
		}
	}
}
