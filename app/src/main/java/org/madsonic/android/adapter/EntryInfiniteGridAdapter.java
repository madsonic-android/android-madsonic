/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.domain.MusicDirectory.Entry;
import org.madsonic.android.domain.ServerInfo;
import org.madsonic.android.fragments.MainFragment;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.service.MusicServiceFactory;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.util.SilentBackgroundTask;
import org.madsonic.android.view.UpdateView;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_LOADING;

public class EntryInfiniteGridAdapter extends EntryGridAdapter {

	private String type;
	private String extra;
	private int size;

	private boolean loading = false;
	private boolean allLoaded = false;

	public EntryInfiniteGridAdapter(Context context, List<Entry> entries, ImageLoader imageLoader, boolean largeCell) {
		super(context, entries, imageLoader, largeCell);
	}

	@Override
	public UpdateView.UpdateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if(viewType == VIEW_TYPE_LOADING) {
			View progress = LayoutInflater.from(context).inflate(R.layout.tab_progress, null);
			progress.setVisibility(View.VISIBLE);
			return new UpdateView.UpdateViewHolder(progress, false);
		}

		return super.onCreateViewHolder(parent, viewType);
	}

	@Override
	public int getItemViewType(int position) {
		if(isLoadingView(position)) {
			return VIEW_TYPE_LOADING;
		}

		return super.getItemViewType(position);
	}

	@Override
	public void onBindViewHolder(UpdateView.UpdateViewHolder holder, int position) {
		if(!isLoadingView(position)) {
			super.onBindViewHolder(holder, position);
		}
	}

	@Override
	public int getItemCount() {
		int size = super.getItemCount();

		if(!allLoaded) {
			size++;
		}

		return size;
	}

	public void setData(String type, String extra, int size) {
		this.type = type;
		this.extra = extra;
		this.size = size;

		if(super.getItemCount() < size) {
			allLoaded = true;
		}
	}

	public void loadMore() {
		if(loading || allLoaded) {
			return;
		}
		loading = true;

		new SilentBackgroundTask<Void>(context) {
			private List<Entry> newData;

			@Override
			protected Void doInBackground() throws Throwable {
				newData = cacheInBackground();
				return null;
			}

			@Override
			protected void done(Void result) {
				appendCachedData(newData);
				loading = false;

				if(newData.size() < size) {
					allLoaded = true;
					notifyDataSetChanged();
				}
			}
		}.execute();
	}

	protected List<Entry> cacheInBackground() throws Exception {
		MusicService service = MusicServiceFactory.getMusicService(context);
		MusicDirectory result = new MusicDirectory();

		int offset = sections.get(0).size();
		if(("genres".equals(type) && ServerInfo.checkServerVersion(context, "1.10.0")) || "years".equals(type)) {
			result = service.getAlbumList(type, extra, size, offset, false, context, null);
		} else if("genres".equals(type) || "genreSongs".equals(type)) {
			result = service.getSongsByGenre(extra, size, offset, context, null);

		} else if("moods".equals(type)) {
			result = service.getSongsByMood(extra, size, offset, context, null);

		} else if("artistGenres".equals(type)) {
			result = service.getArtistsByGenre(extra, size, offset, context, null);

		} else if("loved".equals(type)) {
			// result = service.getLovedList(context, null);

		}else if(type.indexOf(MainFragment.SONGS_LIST_PREFIX) != -1) {
			result = service.getSongList(type, size, offset, context, null);
		} else {
			result = service.getAlbumList(type, size, offset, false, context, null);
		}
		return result.getChildren();
	}

	protected void appendCachedData(List<Entry> newData) {
		if(newData.size() > 0) {
			int start = sections.get(0).size();
			sections.get(0).addAll(newData);
			this.notifyItemRangeInserted(start, newData.size());
		}
	}

	protected boolean isLoadingView(int position) {
		return !allLoaded && position >= sections.get(0).size();
	}
}
