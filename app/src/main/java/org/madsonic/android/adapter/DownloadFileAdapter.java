/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import org.madsonic.android.R;
import org.madsonic.android.service.DownloadFile;
import org.madsonic.android.util.Util;
import org.madsonic.android.view.FastScroller;
import org.madsonic.android.view.SongView;
import org.madsonic.android.view.UpdateView;

import java.util.List;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_DOWNLOAD_FILE;

public class DownloadFileAdapter extends SectionAdapter<DownloadFile> implements FastScroller.BubbleTextGetter {

	public DownloadFileAdapter(Context context, List<DownloadFile> entries, OnItemClickedListener onItemClickedListener) {
		super(context, entries);
		this.onItemClickedListener = onItemClickedListener;
		this.checkable = true;
	}

	@Override
	public UpdateView.UpdateViewHolder onCreateSectionViewHolder(ViewGroup parent, int viewType) {
		return new UpdateView.UpdateViewHolder(new SongView(context));
	}

	@Override
	public void onBindViewHolder(UpdateView.UpdateViewHolder holder, DownloadFile item, int viewType) {
		SongView songView = (SongView) holder.getUpdateView();
		songView.setObject(item.getSong(), Util.isBatchMode(context));
		songView.setDownloadFile(item);
	}

	@Override
	public int getItemViewType(DownloadFile item) {
		return VIEW_TYPE_DOWNLOAD_FILE;
	}

	@Override
	public String getTextToShowInBubble(int position) {
		return null;
	}

	@Override
	public void onCreateActionModeMenu(Menu menu, MenuInflater menuInflater) {
		if(Util.isOffline(context)) {
			menuInflater.inflate(R.menu.multiselect_nowplaying_offline, menu);
		} else {
			menuInflater.inflate(R.menu.multiselect_nowplaying, menu);
		}

		if(!selected.isEmpty()) {
			MenuItem starItem = menu.findItem(R.id.menu_star);
			if(starItem != null) {
				boolean isStarred = selected.get(0).getSong().isStarred();
				starItem.setTitle(isStarred ? R.string.common_unstar : R.string.common_star);
			}
		}
	}
}
