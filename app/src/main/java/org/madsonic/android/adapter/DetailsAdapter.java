/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.madsonic.android.R;

import java.util.List;

public class DetailsAdapter extends ArrayAdapter<String> {
	private List<String> headers;
	private List<String> details;

	public DetailsAdapter(Context context, int layout, List<String> headers, List<String> details) {
		super(context, layout, headers);

		this.headers = headers;
		this.details = details;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view;
		if(convertView == null) {
			view = LayoutInflater.from(getContext()).inflate(R.layout.details_item, null);
		} else {
			view = convertView;
		}

		TextView nameView = (TextView) view.findViewById(R.id.detail_name);
		TextView detailsView = (TextView) view.findViewById(R.id.detail_value);

		nameView.setText(headers.get(position));

		detailsView.setText(details.get(position));
		Linkify.addLinks(detailsView, Linkify.WEB_URLS | Linkify.EMAIL_ADDRESSES);

		return view;
	}
}
