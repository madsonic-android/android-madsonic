/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.madsonic.android.R;
import org.madsonic.android.activity.MadsonicActivity;
import org.madsonic.android.domain.ChatMessage;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.util.UserUtil;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

public class ChatAdapter extends ArrayAdapter<ChatMessage> {
	
	private final MadsonicActivity activity;
	private ArrayList<ChatMessage> messages;
	private final ImageLoader imageLoader;
	
    private static final String phoneRegex = "1?\\W*([2-9][0-8][0-9])\\W*([2-9][0-9]{2})\\W*([0-9]{4})"; //you can just place your support phone here
    private static final Pattern phoneMatcher = Pattern.compile(phoneRegex);

    public ChatAdapter(MadsonicActivity activity, ArrayList<ChatMessage> messages, ImageLoader imageLoader) {
        super(activity, R.layout.chat_item, messages);
        this.activity = activity;
        this.messages = messages;
		this.imageLoader = imageLoader;
    }
    
    @Override
	public int getCount() {
		return messages.size();
	}
    
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ChatMessage message = this.getItem(position);

		ViewHolder holder;
        int layout;
		
        String messageUser = message.getUsername();
        Date messageTime = new java.util.Date(message.getTime());
        String messageText = message.getMessage();
        
        String me = UserUtil.getCurrentUsername(activity);
        
        if (messageUser.equals(me)) {
        	layout = R.layout.chat_item_reverse;
        } else {
        	layout = R.layout.chat_item;
        }
        
		if (convertView == null)
		{
			holder = new ViewHolder();
			
			convertView = LayoutInflater.from(activity).inflate(layout, parent, false);
			
	        TextView usernameView = (TextView) convertView.findViewById(R.id.chat_username);
	        TextView timeView = (TextView) convertView.findViewById(R.id.chat_time);
	        TextView messageView = (TextView) convertView.findViewById(R.id.chat_message);
	        
	        messageView.setMovementMethod(LinkMovementMethod.getInstance());
	        Linkify.addLinks(messageView, Linkify.EMAIL_ADDRESSES);
	        Linkify.addLinks(messageView, Linkify.WEB_URLS);
	        Linkify.addLinks(messageView, phoneMatcher, "tel:");

	        holder.message = messageView;
			holder.username = usernameView;
			holder.time = timeView;
			holder.avatar = (ImageView) convertView.findViewById(R.id.chat_avatar);
			
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(activity);
		String messageTimeFormatted = String.format("[%s]", timeFormat.format(messageTime));
		
      	holder.username.setText(messageUser);
        holder.message.setText(messageText);
    	holder.time.setText(messageTimeFormatted);

		imageLoader.loadAvatar(activity, holder.avatar, messageUser);

		return convertView;
	}
    
	private static class ViewHolder
	{
		TextView message;
		TextView username;
		TextView time;
		ImageView avatar;
	}
}
