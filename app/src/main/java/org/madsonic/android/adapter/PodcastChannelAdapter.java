/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.adapter;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.ViewGroup;

import org.madsonic.android.R;
import org.madsonic.android.domain.PodcastChannel;
import org.madsonic.android.domain.PodcastEpisode;
import org.madsonic.android.util.ImageLoader;
import org.madsonic.android.util.Util;
import org.madsonic.android.view.FastScroller;
import org.madsonic.android.view.PodcastChannelView;
import org.madsonic.android.view.SongView;
import org.madsonic.android.view.UpdateView;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import static org.madsonic.android.view.ViewType.VIEW_TYPE_PODCAST_CELL;
import static org.madsonic.android.view.ViewType.VIEW_TYPE_PODCAST_EPISODE;
import static org.madsonic.android.view.ViewType.VIEW_TYPE_PODCAST_LEGACY;
import static org.madsonic.android.view.ViewType.VIEW_TYPE_PODCAST_LINE;

public class PodcastChannelAdapter extends ExpandableSectionAdapter<Serializable> implements FastScroller.BubbleTextGetter {

	private ImageLoader imageLoader;
	private boolean largeCell;

	public PodcastChannelAdapter(Context context, List<Serializable> podcasts, ImageLoader imageLoader, OnItemClickedListener listener, boolean largeCell) {
		super(context, podcasts);
		this.imageLoader = imageLoader;
		this.onItemClickedListener = listener;
		this.largeCell = largeCell;
	}
	public PodcastChannelAdapter(Context context, List<String> headers, List<List<Serializable>> sections, ImageLoader imageLoader, OnItemClickedListener listener, boolean largeCell) {
		super(context, headers, sections, Arrays.asList(3, null));
		this.imageLoader = imageLoader;
		this.onItemClickedListener = listener;
		this.largeCell = largeCell;
		checkable = true;
	}

	@Override
	public UpdateView.UpdateViewHolder onCreateSectionViewHolder(ViewGroup parent, int viewType) {
		UpdateView updateView;
		if(viewType == VIEW_TYPE_PODCAST_EPISODE) {
			updateView = new SongView(context);
		} else if(viewType == VIEW_TYPE_PODCAST_LEGACY) {
			updateView = new PodcastChannelView(context);
		} else {
			updateView = new PodcastChannelView(context, imageLoader, viewType == VIEW_TYPE_PODCAST_CELL);
		}

		return new UpdateView.UpdateViewHolder(updateView);
	}

	@Override
	public void onBindViewHolder(UpdateView.UpdateViewHolder holder, Serializable item, int viewType) {
		if(viewType == VIEW_TYPE_PODCAST_EPISODE) {
			SongView songView = (SongView) holder.getUpdateView();
			PodcastEpisode episode = (PodcastEpisode) item;
			songView.setShowPodcast(true);
			songView.setObject(episode, !episode.isVideo());
		} else {
			holder.getUpdateView().setObject(item);
		}
	}

	@Override
	public int getItemViewType(Serializable item) {
		if(item instanceof PodcastChannel) {
			PodcastChannel channel = (PodcastChannel) item;

			if (imageLoader != null && channel.getCoverArt() != null) {
				return largeCell ? VIEW_TYPE_PODCAST_CELL : VIEW_TYPE_PODCAST_LINE;
			} else {
				return VIEW_TYPE_PODCAST_LEGACY;
			}
		} else {
			return VIEW_TYPE_PODCAST_EPISODE;
		}
	}

	@Override
	public String getTextToShowInBubble(int position) {
		Serializable item = getItemForPosition(position);
		if(item instanceof PodcastChannel) {
			PodcastChannel channel = (PodcastChannel) item;
			return getNameIndex(channel.getName(), true);
		} else {
			return null;
		}
	}

	@Override
	public void onCreateActionModeMenu(Menu menu, MenuInflater menuInflater) {
		if(Util.isOffline(context)) {
			menuInflater.inflate(R.menu.multiselect_media_offline, menu);
		} else {
			menuInflater.inflate(R.menu.multiselect_media, menu);
		}

		menu.removeItem(R.id.menu_remove_playlist);
		menu.removeItem(R.id.menu_star);
	}
}
