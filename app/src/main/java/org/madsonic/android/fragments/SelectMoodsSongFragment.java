/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.fragments;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.madsonic.android.R;
import org.madsonic.android.adapter.GenreSongAdapter;
import org.madsonic.android.adapter.MoodSongAdapter;
import org.madsonic.android.adapter.SectionAdapter;
import org.madsonic.android.domain.Genre;
import org.madsonic.android.domain.Mood;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.util.Constants;
import org.madsonic.android.util.ProgressListener;
import org.madsonic.android.view.UpdateView;

import java.util.List;

public class SelectMoodsSongFragment extends SelectRecyclerFragment<Mood> {
	private static final String TAG = SelectMoodsSongFragment.class.getSimpleName();

	@Override
	public int getOptionsMenu() {
		return R.menu.empty;
	}

	@Override
	public SectionAdapter getAdapter(List<Mood> objs) {
		return new MoodSongAdapter(context, objs, this);
	}

	@Override
	public List<Mood> getObjects(MusicService musicService, boolean refresh, ProgressListener listener) throws Exception {
		return musicService.getMoods(refresh, "song", context, listener);
	}

	@Override
	public int getTitleResource() {
		return R.string.main_songs_moods;
	}
	
	@Override
	public void onItemClicked(UpdateView<Mood> updateView, Mood mood) {
		MadsonicFragment fragment = new SelectDirectoryFragment();
		Bundle args = new Bundle();
		args.putString(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_TYPE, "moods");
		args.putInt(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_SIZE, 20);
		args.putInt(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_OFFSET, 0);
		args.putString(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_EXTRA, mood.getName());
		fragment.setArguments(args);

		replaceFragment(fragment);
	}

	@Override
	public void onCreateContextMenu(Menu menu, MenuInflater menuInflater, UpdateView<Mood> updateView, Mood item) {}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem, UpdateView<Mood> updateView, Mood item) {
		return false;
	}
}
