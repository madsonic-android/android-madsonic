/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.madsonic.android.R;
import org.madsonic.android.adapter.MainAdapter;
import org.madsonic.android.adapter.SectionAdapter;
import org.madsonic.android.domain.ServerInfo;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.service.MusicServiceFactory;
import org.madsonic.android.util.Constants;
import org.madsonic.android.util.EnvironmentVariables;
import org.madsonic.android.util.FileUtil;
import org.madsonic.android.util.LoadingTask;
import org.madsonic.android.util.ProgressListener;
import org.madsonic.android.util.UserUtil;
import org.madsonic.android.util.Util;
import org.madsonic.android.view.ChangeLog;
import org.madsonic.android.view.FastScroller;
import org.madsonic.android.view.UpdateView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainFragment extends SelectRecyclerFragment<Integer> {
	private static final String TAG = MainFragment.class.getSimpleName();

	public static final String SONGS_LIST_PREFIX = "songs-";
	public static final String SONGS_NEWEST = SONGS_LIST_PREFIX + "newest";
	public static final String SONGS_RECENT = SONGS_LIST_PREFIX + "recent";
    public static final String SONGS_HIGHEST = SONGS_LIST_PREFIX + "highest";
    public static final String SONGS_FREQUENT = SONGS_LIST_PREFIX + "frequent";
    public static final String SONGS_TOP_PLAYED = SONGS_LIST_PREFIX + "topPlayed";

	public MainFragment() {
		super();
		pullToRefresh = false;
		serialize = false;
		backgroundUpdate = false;
		alwaysFullscreen = true;
	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
		menuInflater.inflate(R.menu.main, menu);
		onFinishSetupOptionsMenu(menu);

		try {
            if (!ServerInfo.canUseAbout(context) ) {
                menu.setGroupVisible(R.id.about_server, false);
            }
			if (!ServerInfo.canRescanServer(context) || !UserUtil.isCurrentAdmin()) {
				menu.setGroupVisible(R.id.rescan_server, false);
			}
		} catch(Exception e) {
			Log.w(TAG, "Error on setting madsonic invisible", e);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(super.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {


            case R.id.menu_global_listmode:
                boolean prefDefault = Util.getPreferences(context).getBoolean(Constants.PREFERENCES_KEY_LARGE_ALBUM_ART, false);
                largeAlbums = prefDefault ? false : true;
                int col = prefDefault ? 1 : 2;
                Util.setListMode(context, largeAlbums);
                recyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_recycler);
                recyclerView.setHasFixedSize(false);
                fastScroller = (FastScroller) rootView.findViewById(R.id.fragment_fast_scroller);
                setupScrollList(recyclerView);
                setupLayoutManager(recyclerView, col);
                invalidate();
                return true;

			case R.id.menu_log:
				getLogs();
				return true;
			case R.id.menu_about_client:
				showAboutDialog();
				return true;
            case R.id.menu_about_server:
                showAboutServerDialog();
                return true;
			case R.id.menu_changelog:
				ChangeLog changeLog = new ChangeLog(context, Util.getPreferences(context));
				changeLog.getFullLogDialog().show();
				return true;
			case R.id.menu_faq:
				showFAQDialog();
				return true;
			case R.id.menu_rescan:
				rescanServer();
				return true;
		}

		return false;
	}

	@Override
	public int getOptionsMenu() {
		return 0;
	}

	@Override
	public SectionAdapter getAdapter(List objs) {

		List<List<Integer>> sections = new ArrayList<>();
		List<String> headers = new ArrayList<>();

        List<Integer> personal = new ArrayList<>();
        if(ServerInfo.checkServerVersion(context, "2.7")) {
            personal.add(R.string.main_personal_loved);
        }
        personal.add(R.string.main_personal_starred);

		if(ServerInfo.checkServerVersion(context, "2.7")) {
			personal.add(R.string.main_songs_highest);
		}

		personal.add(R.string.main_personal_bookmarked);
        sections.add(personal);
        headers.add("personal");

		List<Integer> albums = new ArrayList<>();
		albums.add(R.string.main_albums_newest);
		albums.add(R.string.main_albums_random);
		albums.add(R.string.main_albums_genres);
		albums.add(R.string.main_albums_alphabetical_name);
		albums.add(R.string.main_albums_alphabetical_artist);
		if(!Util.isTagBrowsing(context)) {
			albums.add(R.string.main_albums_highest);
		}
		albums.add(R.string.main_albums_year);
		albums.add(R.string.main_albums_recent);
		albums.add(R.string.main_albums_frequent);
		sections.add(albums);
		headers.add("albums");


		List<Integer> songs = new ArrayList<>();
        songs.add(R.string.main_songs_genres);

		if(ServerInfo.checkServerVersion(context, "2.7")) {
			songs.add(R.string.main_songs_moods);
		}

        if(ServerInfo.checkServerVersion(context, "2.5")) {
            songs.add(R.string.main_songs_newest);
            songs.add(R.string.main_songs_recent);
    		songs.add(R.string.main_songs_frequent);
            songs.add(R.string.main_songs_top_played);
        }
		if(ServerInfo.checkServerVersion(context, "2.7")) {
            songs.add(R.string.main_songs_highest);
		}
		sections.add(songs);
		headers.add("songs");

		if(ServerInfo.checkServerVersion(context, "2.5")) {
			List<Integer> artists = new ArrayList<>();
			if(!Util.isTagBrowsing(context)) {
				artists.add(R.string.main_artists_starred);
			}
			artists.add(R.string.main_artists_genre);
			artists.add(R.string.main_artists_all);
			sections.add(artists);
			headers.add("artists");
		}

		List<Integer> videos = Arrays.asList(R.string.main_videos);
		sections.add(videos);
		headers.add("videos");

		return new MainAdapter(context, headers, sections, this);
	}

	@Override
	public List<Integer> getObjects(MusicService musicService, boolean refresh, ProgressListener listener) throws Exception {
		return Arrays.asList(0);
	}

	@Override
	public int getTitleResource() {
		return R.string.common_appname;
	}

	private void showAlbumList(String type) {
		if("genres".equals(type)) {
			MadsonicFragment fragment = new SelectGenreAlbumFragment();
			replaceFragment(fragment);

        } else if("artistGenres".equals(type)) {
            MadsonicFragment fragment = new SelectGenreArtistFragment();
            replaceFragment(fragment);

		} else if("genreSongs".equals(type)) {
			MadsonicFragment fragment = new SelectGenreSongFragment();
			replaceFragment(fragment);

        } else if("moods".equals(type)) {
            MadsonicFragment fragment = new SelectMoodsSongFragment();
            replaceFragment(fragment);

		} else if("years".equals(type)) {
			MadsonicFragment fragment = new SelectYearFragment();
			replaceFragment(fragment);

		} else {
			// Clear out recently added count when viewing
			if("newest".equals(type)) {
				SharedPreferences.Editor editor = Util.getPreferences(context).edit();
				editor.putInt(Constants.PREFERENCES_KEY_RECENT_COUNT + Util.getActiveServer(context), 0);
				editor.commit();
			}
			
			MadsonicFragment fragment = new SelectDirectoryFragment();
			Bundle args = new Bundle();
			args.putString(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_TYPE, type);
			args.putInt(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_SIZE, 20);
			args.putInt(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_OFFSET, 0);
			fragment.setArguments(args);

			replaceFragment(fragment);
		}
	}

	private void showVideos() {
		MadsonicFragment fragment = new SelectVideoFragment();
		replaceFragment(fragment);
	}

    private void showBookmark() {
        MadsonicFragment fragment = new SelectBookmarkFragment();
        replaceFragment(fragment);
    }

	private void showAboutDialog() {
		new LoadingTask<Void>(context) {
			Long[] used;
			long bytesTotalFs;
			long bytesAvailableFs;

			@Override
			protected Void doInBackground() throws Throwable {
				File rootFolder = FileUtil.getMusicDirectory(context);
				StatFs stat = new StatFs(rootFolder.getPath());
				bytesTotalFs = (long) stat.getBlockCount() * (long) stat.getBlockSize();
				bytesAvailableFs = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();

				used = FileUtil.getUsedSize(context, rootFolder);
				return null;
			}

			@Override
			protected void done(Void result) {
				List<Integer> headers = new ArrayList<>();
				List<String> details = new ArrayList<>();

				headers.add(R.string.details_empty);
				details.add("");

				headers.add(R.string.details_author);
				details.add("Madsonic");

				headers.add(R.string.details_email);
				details.add("support@madsonic.org");

				try {
					headers.add(R.string.details_version);
                    final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
					details.add(packageInfo.versionName);

                    headers.add(R.string.details_build);
                    details.add(String.valueOf(packageInfo.versionCode));

				} catch(Exception e) {
					details.add("");
				}

				Resources res = context.getResources();
				headers.add(R.string.details_files_cached);
				details.add(Long.toString(used[0]));

				headers.add(R.string.details_files_permanent);
				details.add(Long.toString(used[1]));

				headers.add(R.string.details_used_space);
				details.add(res.getString(R.string.details_of, Util.formatLocalizedBytes(used[2], context), Util.formatLocalizedBytes(Util.getCacheSizeMB(context) * 1024L * 1024L, context)));

				headers.add(R.string.details_available_space);
				details.add(res.getString(R.string.details_of, Util.formatLocalizedBytes(bytesAvailableFs, context), Util.formatLocalizedBytes(bytesTotalFs, context)));

				Util.showDetailsDialog(context, R.string.main_about_title, headers, details);
			}
		}.execute();
	}

    private void showAboutServerDialog() {


        new LoadingTask<List <ServerInfo>>(context) {

            @Override
            protected List<ServerInfo> doInBackground() throws Throwable {
                MusicService musicService = MusicServiceFactory.getMusicService(context);
                return musicService.getApi(context, this);
            }

            @Override
            protected void done(List <ServerInfo> result) {
                List<Integer> headers = new ArrayList<>();
                List<String> details = new ArrayList<>();

                if (result.get(0).getServerVersion() == null) {
                    headers.add(R.string.details_empty);
                    details.add("");
                    headers.add(R.string.details_server_type);
                    details.add("Madsonic");
                    headers.add(R.string.details_api_version);
                    details.add(String.valueOf(result.get(0).getRestType() == 3 ? "REST " + result.get(1).getRestVersion() : "REST " + result.get(0).getRestVersion()));
                } else {
                    headers.add(R.string.details_empty);
                    details.add("");
                    headers.add(R.string.details_server_type);
                    details.add("Madsonic");
                    headers.add(R.string.details_server_version);
                    details.add(String.valueOf(result.get(0).getServerVersion()));
                    headers.add(R.string.details_api_version);
                    details.add(String.valueOf(result.get(1).getRestType() == 3 ? "REST " + result.get(1).getRestVersion() : "REST " + result.get(1).getRestVersion()));
                    headers.add(R.string.details_license);
                    details.add(String.valueOf(result.get(0).isLicenseValid()));
                }
                Util.showDetailsDialog(context, R.string.main_about_title, headers, details);
            }
        }.execute();


    }

	private void showFAQDialog() {
		Util.showHTMLDialog(context, R.string.main_faq_title, R.string.main_faq_text);
	}

	private void rescanServer() {
		new LoadingTask<Void>(context, false) {
			@Override
			protected Void doInBackground() throws Throwable {
				MusicService musicService = MusicServiceFactory.getMusicService(context);
				musicService.startRescan(context, this);
				return null;
			}

			@Override
			protected void done(Void value) {
				Util.toast(context, R.string.main_scan_complete);
			}
		}.execute();
	}

	private void getLogs() {
		try {
			final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			new LoadingTask<String>(context) {
				@Override
				protected String doInBackground() throws Throwable {
					updateProgress("Gathering Logs");
					File logcat = new File(Environment.getExternalStorageDirectory(), "madsonic-logcat.txt");
					Util.delete(logcat);
					Process logcatProc = null;

					try {
						List<String> progs = new ArrayList<String>();
						progs.add("logcat");
						progs.add("-v");
						progs.add("time");
						progs.add("-d");
						progs.add("-f");
						progs.add(logcat.getCanonicalPath());
						progs.add("*:I");

						logcatProc = Runtime.getRuntime().exec(progs.toArray(new String[progs.size()]));
						logcatProc.waitFor();
					} finally {
						if(logcatProc != null) {
							logcatProc.destroy();
						}
					}

					URL url = new URL("https://pastebin.com/api/api_post.php");
					HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
					StringBuffer responseBuffer = new StringBuffer();
					try {
						urlConnection.setReadTimeout(10000);
						urlConnection.setConnectTimeout(15000);
						urlConnection.setRequestMethod("POST");
						urlConnection.setDoInput(true);
						urlConnection.setDoOutput(true);

						OutputStream os = urlConnection.getOutputStream();
						BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, Constants.UTF_8));
						writer.write("api_dev_key=" + URLEncoder.encode(EnvironmentVariables.PASTEBIN_DEV_KEY, Constants.UTF_8) + "&api_option=paste&api_paste_private=2&api_paste_expire_date=1M&api_paste_code=");

						BufferedReader reader = null;
						try {
							reader = new BufferedReader(new InputStreamReader(new FileInputStream(logcat)));
							String line;
							while ((line = reader.readLine()) != null) {
								writer.write(URLEncoder.encode(line.replaceFirst("&p=.*?&", "&p=***&").replaceFirst("&t=.*?&", "&t=***&") + "\n", Constants.UTF_8));
							}
						} finally {
							Util.close(reader);
						}

						File stacktrace = new File(Environment.getExternalStorageDirectory(), "madsonic-stacktrace.txt");
						if(stacktrace.exists() && stacktrace.isFile()) {
							writer.write("\n\nMost Recent Stacktrace:\n\n");

							reader = null;
							try {
								reader = new BufferedReader(new InputStreamReader(new FileInputStream(stacktrace)));
								String line;
								while ((line = reader.readLine()) != null) {
									writer.write(URLEncoder.encode(line + "\n", Constants.UTF_8));
								}
							} finally {
								Util.close(reader);
							}
						}

						writer.flush();
						writer.close();
						os.close();

						BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
						String inputLine;
						while ((inputLine = in.readLine()) != null) {
							responseBuffer.append(inputLine);
						}
						in.close();
					} finally {
						urlConnection.disconnect();
					}

					String response = responseBuffer.toString();
					if(response.indexOf("http") == 0) {
						return response.replace("http:", "https:");
					} else {
						throw new Exception("Pastebin Error: " + response);
					}
				}

				@Override
				protected void error(Throwable error) {
					Log.e(TAG, "Failed to gather logs", error);
					Util.toast(context, "Failed to gather logs");
				}

				@Override
				protected void done(String logcat) {
					String footer = "Android SDK: " + Build.VERSION.SDK;
					footer += "\nDevice Model: " + Build.MODEL;
					footer += "\nDevice Name: " + Build.MANUFACTURER + " "  + Build.PRODUCT;
					footer += "\nROM: " + Build.DISPLAY;
					footer += "\nLogs: " + logcat;
					footer += "\nBuild Number: " + packageInfo.versionCode;

					Intent email = new Intent(Intent.ACTION_SENDTO,
					Uri.fromParts("mailto", "support@madsonic.org", null));
					email.putExtra(Intent.EXTRA_SUBJECT, "Madsonic " + packageInfo.versionName + " Error Logs");
					email.putExtra(Intent.EXTRA_TEXT, "Describe the problem here\n\n\n" + footer);
					startActivity(email);
				}
			}.execute();
		} catch(Exception e) {}
	}

	@Override
	public void onItemClicked(UpdateView<Integer> updateView, Integer item) {

        if (item == R.string.main_artists_starred) {
            showAlbumList("starredArtist");
        } else if (item == R.string.main_artists_genre) {
            showAlbumList("artistGenres");
        } else if (item == R.string.main_artists_all) {
            showAlbumList("allArtist");
        } else if (item == R.string.main_albums_newest) {
			showAlbumList("newest");
		} else if (item == R.string.main_albums_random) {
			showAlbumList("random");
		} else if (item == R.string.main_albums_highest) {
			showAlbumList("highest");
		} else if (item == R.string.main_albums_recent) {
			showAlbumList("recent");
		} else if (item == R.string.main_albums_frequent) {
			showAlbumList("frequent");
		} else if (item == R.string.main_personal_starred) {
			showAlbumList("starred");
        } else if (item == R.string.main_personal_loved) {
            showAlbumList("loved");
        } else if(item == R.string.main_personal_bookmarked) {
            showBookmark();
		} else if(item == R.string.main_albums_genres) {
			showAlbumList("genres");
        } else if (item == R.string.main_songs_moods) {
            showAlbumList("moods");
		} else if(item == R.string.main_albums_year) {
			showAlbumList("years");
		} else if(item == R.string.main_albums_alphabetical_name) {
			showAlbumList("alphabeticalByName");
        } else if(item == R.string.main_albums_alphabetical_artist) {
            showAlbumList("alphabeticalByArtist");
		} else if(item == R.string.main_videos) {
			showVideos();
		} else if (item == R.string.main_songs_newest) {
			showAlbumList(SONGS_NEWEST);
		} else if (item == R.string.main_songs_top_played) {
			showAlbumList(SONGS_TOP_PLAYED);

		} else if (item == R.string.main_songs_genres) {
			showAlbumList("genreSongs");

		} else if (item == R.string.main_songs_recent) {
			showAlbumList(SONGS_RECENT);
		} else if (item == R.string.main_songs_frequent) {
			showAlbumList(SONGS_FREQUENT);
        } else if (item == R.string.main_songs_highest) {
            showAlbumList(SONGS_HIGHEST);
		}
	}

	@Override
	public void onCreateContextMenu(Menu menu, MenuInflater menuInflater, UpdateView<Integer> updateView, Integer item) {}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem, UpdateView<Integer> updateView, Integer item) {
		return false;
	}
}
