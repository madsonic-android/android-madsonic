/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.fragments;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.adapter.InternetRadioStationAdapter;
import org.madsonic.android.adapter.SectionAdapter;
import org.madsonic.android.domain.InternetRadioStation;
import org.madsonic.android.service.DownloadService;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.service.MusicServiceFactory;
import org.madsonic.android.service.OfflineException;
import org.madsonic.android.service.ServerTooOldException;
import org.madsonic.android.util.LoadingTask;
import org.madsonic.android.util.ProgressListener;
import org.madsonic.android.util.TabBackgroundTask;
import org.madsonic.android.util.Util;
import org.madsonic.android.view.UpdateView;

public class SelectInternetRadioStationFragment extends SelectRecyclerFragment<InternetRadioStation> {
	private static final String TAG = SelectInternetRadioStationFragment.class.getSimpleName();

	@Override
	public int getOptionsMenu() {
		return R.menu.select_internet_radio;
	}

	@Override
	public SectionAdapter<InternetRadioStation> getAdapter(List<InternetRadioStation> objs) {
		return new InternetRadioStationAdapter(context, objs, this);
	}

	@Override
	public List<InternetRadioStation> getObjects(MusicService musicService, boolean refresh, ProgressListener listener) throws Exception {
		return musicService.getInternetRadioStations(refresh, context, listener);
	}

	@Override
	public int getTitleResource() {
		return R.string.button_bar_internet_radio;
	}

	@Override
	public void onItemClicked(UpdateView<InternetRadioStation> updateView, final InternetRadioStation item) {
		new TabBackgroundTask<Void>(this) {
			@Override
			protected Void doInBackground() throws Throwable {
				DownloadService downloadService = getDownloadService();
				if(downloadService == null) {
					return null;
				}

				getStreamFromPlaylist(item);
				downloadService.download(item);
				return null;
			}

			@Override
			protected void done(Void result) {
				context.openNowPlaying();
			}
		}.execute();
	}

	@Override
	public void onCreateContextMenu(Menu menu, MenuInflater menuInflater, UpdateView<InternetRadioStation> updateView, InternetRadioStation item) {
		menuInflater.inflate(R.menu.select_internet_radio_context, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem, UpdateView<InternetRadioStation> updateView, InternetRadioStation item) {
		switch (menuItem.getItemId()) {

			case R.id.internet_radio_info:
				displayInternetRadioStationInfo(item);
				break;
		}

		return false;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(super.onOptionsItemSelected(item)) {
            return true;
        }
       switch (item.getItemId()) {
             case R.id.menu_add_radio:
                addNewRadio();
                break;
        }
        return false;
    }

	private void getStreamFromPlaylist(InternetRadioStation internetRadioStation) {
		if(internetRadioStation.getStreamUrl() != null && (internetRadioStation.getStreamUrl().indexOf(".m3u") != -1 || internetRadioStation.getStreamUrl().indexOf(".pls") != -1)) {
			try {
				URL url = new URL(internetRadioStation.getStreamUrl());
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();

				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					String line;
					while((line = in.readLine()) != null) {
						// Not blank line or comment
						if(line.length() > 0 && line.indexOf('#') != 0) {
							if(internetRadioStation.getStreamUrl().indexOf(".m3u") != -1) {
								internetRadioStation.setStreamUrl(line);
								break;
							} else {
								if(line.indexOf("File1=") == 0) {
									internetRadioStation.setStreamUrl(line.replace("File1=", ""));
								} else if(line.indexOf("Title1=") == 0) {
									internetRadioStation.setTitle(line.replace("Title1=", ""));
								}
							}
						}
					}
				} finally {
					connection.disconnect();
				}
			} catch (Exception e) {
				Log.e(TAG, "Failed to get stream data from playlist", e);
			}

		}
	}

	private void addNewRadio() {
		View dialogView = context.getLayoutInflater().inflate(R.layout.create_radio, null);

        final TextView nameBox = (TextView) dialogView.findViewById(R.id.create_radio_name);
		final TextView streamUrlBox = (TextView) dialogView.findViewById(R.id.create_radio_stream_url);
		final TextView homepageUrlBox = (TextView) dialogView.findViewById(R.id.create_radio_homepage_url);

		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		//builder.setTitle(R.string.menu_add_radio)

        builder.setView(dialogView)
				.setPositiveButton(R.string.common_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						addNewRadio(nameBox.getText().toString(), streamUrlBox.getText().toString(), homepageUrlBox.getText().toString());
					}
				})
				.setNegativeButton(R.string.common_cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				})
				.setCancelable(true);

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void addNewRadio(final String name, final String streamUrl, final String homepageUrl) {
		new LoadingTask<Void>(context, false) {
			@Override
			protected Void doInBackground() throws Throwable {
				MusicService musicService = MusicServiceFactory.getMusicService(context);
				musicService.createInternetRadio(name, streamUrl, homepageUrl, context, null);
				return null;
			}

			@Override
			protected void done(Void result) {
				refresh();
			}

			@Override
			protected void error(Throwable error) {
				String msg;
				if (error instanceof OfflineException || error instanceof ServerTooOldException) {
					msg = getErrorMessage(error);
				} else {
					msg = context.getResources().getString(R.string.select_radio_created_error) + " " + getErrorMessage(error);
				}

				Util.toast(context, msg, false);
			}
		}.execute();
	}

	private void displayInternetRadioStationInfo(final InternetRadioStation station) {
		List<Integer> headers = new ArrayList<>();
		List<String> details = new ArrayList<>();

		headers.add(R.string.details_title);
		details.add(station.getTitle());

		headers.add(R.string.details_home_page);
		details.add(station.getHomePageUrl());

		headers.add(R.string.details_stream_url);
		details.add(station.getStreamUrl());

		Util.showDetailsDialog(context, R.string.details_title_internet_radio_station, headers, details);
	}
}
