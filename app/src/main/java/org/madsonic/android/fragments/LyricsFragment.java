/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.madsonic.android.R;
import org.madsonic.android.domain.Lyrics;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.service.MusicServiceFactory;
import org.madsonic.android.util.BackgroundTask;
import org.madsonic.android.util.Constants;
import org.madsonic.android.util.TabBackgroundTask;

/**
 * Displays song lyrics.
 */
public final class LyricsFragment extends MadsonicFragment {
	private TextView artistView;
	private TextView titleView;
	private TextView textView;

	private Lyrics lyrics;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
        //Bridge.restoreInstanceState(this, bundle);
		if(bundle != null) {
			lyrics = (Lyrics) bundle.getSerializable(Constants.FRAGMENT_LIST);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(Constants.FRAGMENT_LIST, lyrics);
		//Bridge.saveInstanceState(this, outState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
		setTitle(R.string.download_menu_lyrics);
		setSubtitle(null);
		rootView = inflater.inflate(R.layout.lyrics, container, false);
		artistView = (TextView) rootView.findViewById(R.id.lyrics_artist);
		titleView = (TextView) rootView.findViewById(R.id.lyrics_title);
		textView = (TextView) rootView.findViewById(R.id.lyrics_text);

		if(lyrics == null) {
			load();
		} else {
			setLyrics();
		}

		return rootView;
	}

	private void load() {
		BackgroundTask<Lyrics> task = new TabBackgroundTask<Lyrics>(this) {
			@Override
			protected Lyrics doInBackground() throws Throwable {
				String artist = getArguments().getString(Constants.INTENT_EXTRA_NAME_ARTIST);
				String title = getArguments().getString(Constants.INTENT_EXTRA_NAME_TITLE);
				MusicService musicService = MusicServiceFactory.getMusicService(context);
				return musicService.getLyrics(artist, title, context, this);
			}

			@Override
			protected void done(Lyrics result) {
				lyrics = result;
				setLyrics();
			}
		};
		task.execute();
	}

	private void setLyrics() {
		if (lyrics != null && lyrics.getArtist() != null) {
			artistView.setText(lyrics.getArtist());
			titleView.setText(lyrics.getTitle());
			textView.setText(lyrics.getText());
		} else {
			artistView.setText(R.string.lyrics_nomatch);
		}
	}
}