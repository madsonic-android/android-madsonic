/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.fragments;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.adapter.BasicListAdapter;
import org.madsonic.android.adapter.SectionAdapter;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.util.Constants;
import org.madsonic.android.util.ProgressListener;
import org.madsonic.android.view.UpdateView;

public class SelectYearFragment extends SelectRecyclerFragment<String> {

	public SelectYearFragment() {
		super();
		pullToRefresh = false;
		serialize = false;
		backgroundUpdate = false;
	}

	@Override
	public int getOptionsMenu() {
		return R.menu.empty;
	}

	@Override
	public SectionAdapter getAdapter(List<String> objs) {
		return new BasicListAdapter(context, objs, this);
	}

	@Override
	public List<String> getObjects(MusicService musicService, boolean refresh, ProgressListener listener) throws Exception {
		List<String> decades = new ArrayList<>();
		for(int i = 2010; i >= 1800; i -= 10) {
			decades.add(String.valueOf(i));
		}

		return decades;
	}

	@Override
	public int getTitleResource() {
		return R.string.main_albums_year;
	}

	@Override
	public void onItemClicked(UpdateView<String> updateView, String decade) {
		MadsonicFragment fragment = new SelectDirectoryFragment();
		Bundle args = new Bundle();
		args.putString(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_TYPE, "years");
		args.putInt(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_SIZE, 20);
		args.putInt(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_OFFSET, 0);
		args.putString(Constants.INTENT_EXTRA_NAME_ALBUM_LIST_EXTRA, decade);
		fragment.setArguments(args);

		replaceFragment(fragment);
	}

	@Override
	public void onCreateContextMenu(Menu menu, MenuInflater menuInflater, UpdateView<String> updateView, String item) {}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem, UpdateView<String> updateView, String item) {
		return false;
	}
}
