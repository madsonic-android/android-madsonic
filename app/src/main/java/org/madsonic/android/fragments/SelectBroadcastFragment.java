/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.fragments;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

import org.madsonic.android.R;
import org.madsonic.android.adapter.BroadcastAdapter;
import org.madsonic.android.adapter.ChatAdapter;
import org.madsonic.android.adapter.EntryGridAdapter;
import org.madsonic.android.adapter.SectionAdapter;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.service.DownloadService;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.util.MenuUtil;
import org.madsonic.android.util.ProgressListener;
import org.madsonic.android.util.Util;
import org.madsonic.android.view.UpdateView;

import java.util.Arrays;
import java.util.List;

public class SelectBroadcastFragment extends SelectRecyclerFragment<MusicDirectory.Entry> {
	private static final String TAG = SelectBroadcastFragment.class.getSimpleName();

	@Override
	public void onCreateContextMenu(Menu menu, MenuInflater menuInflater, UpdateView<MusicDirectory.Entry> updateView, MusicDirectory.Entry item) {
		menuInflater.inflate(R.menu.select_broadcast_context, menu);
		MenuUtil.hideMenuItems(context, menu, updateView);
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem, UpdateView<MusicDirectory.Entry> updateView, MusicDirectory.Entry songs) {
		switch(menuItem.getItemId()) {
		    //TODO: MENU CALLBACKS
		}
		return onContextItemSelected(menuItem, songs);
	}

	@Override
	public int getOptionsMenu() {
		return R.menu.abstract_top_menu;
	}

    @Override
    public SectionAdapter getAdapter(List<MusicDirectory.Entry> songs) {
        SectionAdapter adapter = new EntryGridAdapter(context, songs, getImageLoader(), false);
        adapter.setOnItemClickedListener(this);
        return adapter;
    }

	@Override
	public List<MusicDirectory.Entry> getObjects(MusicService musicService, boolean refresh, ProgressListener listener) throws Exception {
		return musicService.getNowplayingSongs(context, listener).getChildren();
	}

	@Override
	public int getTitleResource() {
		return R.string.button_bar_broadcast;
	}

	@Override
	public void onItemClicked(UpdateView<MusicDirectory.Entry> updateView, final MusicDirectory.Entry songs) {
		final DownloadService downloadService = getDownloadService();
		if(downloadService == null) {
			return;
		}

		boolean allowPlayAll = ((!Util.isTagBrowsing(context)) || (Util.isTagBrowsing(context) && songs.getAlbumId() != null)) && !songs.isPodcast();
		if(allowPlayAll && "all".equals(Util.getSongPressAction(context))) {
			onSongPress(Arrays.asList(songs), songs, false);
		}
	}
}
