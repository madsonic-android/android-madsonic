/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.fragments;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.adapter.EntryGridAdapter;
import org.madsonic.android.adapter.SectionAdapter;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.util.ProgressListener;
import org.madsonic.android.view.UpdateView;

public class SelectVideoFragment extends SelectRecyclerFragment<MusicDirectory.Entry> {
	@Override
	public int getOptionsMenu() {
		return R.menu.empty;
	}

	@Override
	public SectionAdapter getAdapter(List<MusicDirectory.Entry> objs) {
		SectionAdapter adapter = new EntryGridAdapter(context, objs, null, false);
		adapter.setOnItemClickedListener(this);
		return adapter;
	}

	@Override
	public List<MusicDirectory.Entry> getObjects(MusicService musicService, boolean refresh, ProgressListener listener) throws Exception {
		MusicDirectory dir = musicService.getVideos(refresh, context, listener);
		return dir.getChildren();
	}

	@Override
	public int getTitleResource() {
		return R.string.main_videos;
	}

	@Override
	public void onItemClicked(UpdateView<MusicDirectory.Entry> updateView, MusicDirectory.Entry entry) {
		playVideo(entry);
	}

	@Override
	public void onCreateContextMenu(Menu menu, MenuInflater menuInflater, UpdateView<MusicDirectory.Entry> updateView, MusicDirectory.Entry item) {
		onCreateContextMenuSupport(menu, menuInflater, updateView, item);
		recreateContextMenu(menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem menuItem, UpdateView<MusicDirectory.Entry> updateView, MusicDirectory.Entry entry) {
		return onContextItemSelected(menuItem, entry);
	}
}
