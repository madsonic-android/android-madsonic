/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service.parser;

import android.content.Context;

import org.madsonic.android.domain.Node;
import org.xmlpull.v1.XmlPullParser;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Karel
 */
public class NodeParser extends AbstractParser {

    public NodeParser(Context context, int instance) {
        super(context, instance);
    }

    public List<Node> parse(Reader reader) throws Exception {

        init(reader);

        List<Node> result = new ArrayList<Node>();
        int eventType;
        do {
            eventType = nextParseEvent();
            if (eventType == XmlPullParser.START_TAG) {
                String name = getElementName();
                if ("node".equals(name)) {
                    String nodeName = get("name");
                    String nodeUrl = get("url");
                    Boolean nodeOnline = getBoolean("enabled");
                    Boolean nodeEnabled =getBoolean("online");

                    result.add(new Node(nodeName, nodeUrl, nodeOnline, nodeEnabled));

                } else if ("error".equals(name)) {
                    handleError();
                }
            }
        } while (eventType != XmlPullParser.END_DOCUMENT);

        validate();

        return result;
    }
}