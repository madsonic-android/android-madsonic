/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service.parser;

import android.content.Context;
import android.text.Html;
import android.util.Log;

import org.madsonic.android.domain.Genre;
import org.madsonic.android.domain.Mood;
import org.madsonic.android.util.ProgressListener;
import org.xmlpull.v1.XmlPullParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Karel
 */
public class MoodParser extends AbstractParser {

	private static final String TAG = MoodParser.class.getSimpleName();

    public MoodParser(Context context, int instance) {
		super(context, instance);
	}

    public List<Mood> parse(Reader reader, ProgressListener progressListener) throws Exception {
        List<Mood> result = new ArrayList<Mood>();
        StringReader sr = null;
        
        try {
        	BufferedReader br = new BufferedReader(reader);
        	String xml = null;
        	String line = null;
        
        	while ((line = br.readLine()) != null) {
        		if (xml == null) {
        			xml = line;
        		} else {
        			xml += line;
        		}
        	}
        	br.close();
        	
        	// Replace double escaped ampersand (&amp;apos;) 
        	xml = xml.replaceAll("(?:&amp;)(amp;|lt;|gt;|#37;|apos;)", "&$1");
        	
            // Replace unescaped ampersand
            xml = xml.replaceAll("&(?!amp;|lt;|gt;|#37;|apos;)", "&amp;");

            // Replace unescaped percent symbol
            // No replacements for <> at this time
            xml = xml.replaceAll("%", "&#37;");
            
            xml = xml.replaceAll("'", "&apos;");
            
            sr = new StringReader(xml);
        } catch (IOException ioe) {
        	Log.e(TAG, "Error parsing Mood XML", ioe);
        }

        if (sr == null) {
        	Log.w(TAG, "Unable to parse Mood XML, returning empty list");
        	return result;
        }
        
        init(sr);

        Mood mood = null;
        
        int eventType;
        do {
            eventType = nextParseEvent();
            if (eventType == XmlPullParser.START_TAG) {
                String name = getElementName();
                if ("mood".equals(name)) {
                    mood = new Mood();
					mood.setSongCount(getInteger("songCount"));
                } else if ("error".equals(name)) {
                    handleError();
                } else {
                	mood = null;
                }
            } else if (eventType == XmlPullParser.TEXT) {
                if (mood != null) {
                	String value = getText();
                	if (mood != null) {
                		mood.setName(Html.fromHtml(value).toString());
                		mood.setIndex(value.substring(0, 1));
                		result.add(mood);
                		mood = null;
                	}
                }
            }
        } while (eventType != XmlPullParser.END_DOCUMENT);

        validate();
        
        return Mood.MoodComparator.sort(result);
    }
}
