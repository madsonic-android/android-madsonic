/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import org.madsonic.android.receiver.HeadphonePlugReceiver;
import org.madsonic.android.util.Util;

/**
 * Created by Scott on 4/6/2015.
 */
public class HeadphoneListenerService extends Service {
	private HeadphonePlugReceiver receiver;

	@Override
	public void onCreate() {
		super.onCreate();

		receiver = new HeadphonePlugReceiver();
		registerReceiver(receiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(!Util.shouldStartOnHeadphones(this)) {
			stopSelf();
		}

		return Service.START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		try {
			if(receiver != null) {
				unregisterReceiver(receiver);
			}
		} catch(Exception e) {
			// Don't care
		}
	}
}
