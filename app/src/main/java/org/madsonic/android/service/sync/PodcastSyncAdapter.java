/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service.sync;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import org.madsonic.android.R;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.domain.PodcastEpisode;
import org.madsonic.android.service.DownloadFile;
import org.madsonic.android.service.parser.MadsonicRESTException;
import org.madsonic.android.util.Notification.NotificationBase;
import org.madsonic.android.util.SyncUtil;
import org.madsonic.android.util.SyncUtil.SyncSet;
import org.madsonic.android.util.FileUtil;
import org.madsonic.android.util.Util;

/**
 * Created by Scott on 8/28/13.
 */

public class PodcastSyncAdapter extends MadsonicSyncAdapter {
	private static String TAG = PodcastSyncAdapter.class.getSimpleName();

	public PodcastSyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
	}
	@TargetApi(14)
	public PodcastSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
		super(context, autoInitialize, allowParallelSyncs);
	}

	@Override
	public void onExecuteSync(Context context, int instance) throws NetworkNotValidException {
		ArrayList<SyncSet> podcastList = SyncUtil.getSyncedPodcasts(context, instance);

		try {
			// Only refresh if syncs exist (implies a server where supported)
			if(podcastList.size() > 0) {
				// Just update podcast listings so user doesn't have to
				musicService.getPodcastChannels(true, context, null);

				// Refresh podcast listings before syncing
				musicService.refreshPodcasts(context, null);
			}

			List<String> updated = new ArrayList<String>();
			String updatedId = null;
			for(int i = 0; i < podcastList.size(); i++) {
				SyncSet set = podcastList.get(i);
				String id = set.id;
				List<String> existingEpisodes = set.synced;
				try {
					MusicDirectory podcasts = musicService.getPodcastEpisodes(true, id, context, null);

					for(MusicDirectory.Entry entry: podcasts.getChildren()) {
						// Make sure podcast is valid and not already synced
						if(entry.getId() != null && "completed".equals(((PodcastEpisode)entry).getStatus()) && !existingEpisodes.contains(entry.getId())) {
							DownloadFile file = new DownloadFile(context, entry, false);
							while(!file.isCompleteFileAvailable() && !file.isFailedMax()) {
								throwIfNetworkInvalid();
								file.downloadNow(musicService);
							}
							// Only add if actualy downloaded correctly
							if(file.isCompleteFileAvailable()) {
								existingEpisodes.add(entry.getId());
								if(!updated.contains(podcasts.getName())) {
									updated.add(podcasts.getName());
									if(updatedId == null) {
										updatedId = podcasts.getId();
									}
								}
							}
						}
					}
				}  catch(MadsonicRESTException e) {
					if(e.getCode() == 70) {
						SyncUtil.removeSyncedPodcast(context, id, instance);
						Log.i(TAG, "Unsync deleted podcasts for " + id + " on " + Util.getServerName(context, instance));
					}
				} catch (Exception e) {
					Log.w(TAG, "Failed to get podcasts for " + id + " on " + Util.getServerName(context, instance));
				}
			}

			// Make sure there are is at least one change before re-syncing
			if(updated.size() > 0) {
				FileUtil.serialize(context, podcastList, SyncUtil.getPodcastSyncFile(context, instance));
				NotificationBase.showSyncNotification(context, R.string.sync_new_podcasts, SyncUtil.joinNames(updated), updatedId);
			}
		} catch(Exception e) {
			Log.w(TAG, "Failed to get podcasts for " + Util.getServerName(context, instance));
		}
	}
}
