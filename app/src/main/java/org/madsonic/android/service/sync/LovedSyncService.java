/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class LovedSyncService extends Service {

	private static LovedSyncAdapter lovedSyncAdapter;
	private static final Object syncLock = new Object();

	@Override
	public void onCreate() {
		synchronized (syncLock) {
			if(lovedSyncAdapter == null) {
				lovedSyncAdapter = new LovedSyncAdapter(getApplicationContext(), true);
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return lovedSyncAdapter.getSyncAdapterBinder();

	}
}
