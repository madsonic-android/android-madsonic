/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service.parser;

import android.content.Context;

import org.xmlpull.v1.XmlPullParser;

import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.domain.PlayerQueue;
import org.madsonic.android.util.ProgressListener;

public class PlayQueueParser extends MusicDirectoryEntryParser {
	private static final String TAG = PlayQueueParser.class.getSimpleName();

	public PlayQueueParser(Context context, int instance) {
		super(context, instance);
	}

	public PlayerQueue parse(Reader reader, ProgressListener progressListener) throws Exception {
		init(reader);

		PlayerQueue state = new PlayerQueue();
		String currentId = null;
		int eventType;
		do {
			eventType = nextParseEvent();
			if (eventType == XmlPullParser.START_TAG) {
				String name = getElementName();
				if("playQueue".equals(name)) {
					currentId = get("current");
					state.currentPlayingPosition = getInteger("position");
					try {
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
						dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
						state.changed = dateFormat.parse(get("changed"));
					} catch (ParseException e) {
						state.changed = null;
					}
				} else if ("entry".equals(name)) {
					MusicDirectory.Entry entry = parseEntry("");
					// Only add songs
					if(!entry.isVideo()) {
						state.songs.add(entry);
					}
				} else if ("error".equals(name)) {
					handleError();
				}
			}
		} while (eventType != XmlPullParser.END_DOCUMENT);

		if(currentId != null) {
			for (MusicDirectory.Entry entry : state.songs) {
				if (entry.getId().equals(currentId)) {
					state.currentPlayingIndex = state.songs.indexOf(entry);
				}
			}
		} else {
			state.currentPlayingIndex = 0;
			state.currentPlayingPosition = 0;
		}

		validate();
		return state;
	}
}
