/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service.parser;

import android.content.Context;
import org.xmlpull.v1.XmlPullParser;

import java.io.Reader;

import org.madsonic.android.R;
import org.madsonic.android.domain.ServerInfo;
import org.madsonic.android.util.ProgressListener;

public class ScanStatusParser extends AbstractParser {

	public ScanStatusParser(Context context, int instance) {
		super(context, instance);
	}

	public boolean parse(Reader reader, ProgressListener progressListener) throws Exception {
		init(reader);

		String scanName, scanningName;
		if(ServerInfo.isMadsonic(context, instance)) {
            scanName = "scan";
            scanningName = "status";
		} else {
            scanName = "scanStatus";
            scanningName = "scanning";
		}
        String msg = "";
		Boolean scanning = null;
		int eventType;
		do {
			eventType = nextParseEvent();
			if (eventType == XmlPullParser.START_TAG) {
				String name = getElementName();
				if(scanName.equals(name)) {

					if (scanName.equals("scanStatus")) {
						scanning = getBoolean(scanningName);
					} else {
						scanning = get(scanningName).equals("scanning");
					}
					if (scanning) {msg = context.getResources().getString(R.string.parser_scan_count, getInteger("count")); }

					progressListener.updateProgress(msg);
				} else if ("error".equals(name)) {
					handleError();
				}
			}
		} while (eventType != XmlPullParser.END_DOCUMENT);

		validate();

		return scanning != null && scanning;
	}
}