/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */
package org.madsonic.android.service.parser;

import android.content.Context;

import org.madsonic.android.domain.ServerInfo;
import org.madsonic.android.domain.Version;
import org.xmlpull.v1.XmlPullParser;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sindre Mehus, Martin Karel
 */
public class InfoParser extends AbstractParser {

    public static final int TYPE_SUBSONIC_REST1 = 1;
    public static final int TYPE_MADSONIC_REST2 = 2;
    public static final int TYPE_MADSONIC_REST3 = 3;

    public InfoParser(Context context, int instance) {
		super(context, instance);
	}

    public List<ServerInfo> parse(Reader reader) throws Exception {

        init(reader);
        List<ServerInfo> result = new ArrayList<ServerInfo>();

        int eventType;
        do {
            eventType = nextParseEvent();
            if (eventType == XmlPullParser.START_TAG) {
                String name = getElementName();

                if ("server".equals(name)) {
                    ServerInfo serverInfo = new ServerInfo();
                    serverInfo.setServerVersion(new Version(get("version")));
                    serverInfo.setLicenseValid(getBoolean("valid"));
                    result.add(serverInfo);
                }
                if ("rest".equals(name)) {
                	String type = get ("type");
                	String version = get ("version");
                    ServerInfo serverInfo = new ServerInfo();
                	 if ("madsonic".equals(type)) {
                         if (version.startsWith("3")) {
                             serverInfo.setRestType(TYPE_MADSONIC_REST3);

                         } else if (version.startsWith("2")) {
                             serverInfo.setRestType(TYPE_MADSONIC_REST2);
                         }
                         serverInfo.setRestVersion(new Version(version));

                     } else if ("subsonic".equals(type)) {
                         serverInfo.setRestType(TYPE_SUBSONIC_REST1);
                         serverInfo.setRestVersion(new Version(version));

                     }
                     result.add(serverInfo);
                }

            }
        } while (eventType != XmlPullParser.END_DOCUMENT);
        validate();

        return result;
    }
}
