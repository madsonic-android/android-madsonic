/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service;

import android.content.Context;
import android.util.Log;

import org.madsonic.android.domain.InternetRadioStation;
import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.domain.PodcastEpisode;
import org.madsonic.android.util.SilentBackgroundTask;
import org.madsonic.android.util.SongDBHandler;
import org.madsonic.android.util.Util;

/**
 * Scrobbles played songs to Last.fm.
 *
 * @author Sindre Mehus
 * @version $Id$
 */
public class Scrobbler {
	private static final String TAG = Scrobbler.class.getSimpleName();
	private static final int FOUR_MINUTES = 4 * 60 * 1000;

	private String lastSubmission;
	private String lastNowPlaying;

	public void conditionalScrobble(Context context, DownloadFile song, int playerPosition, int duration, boolean isPastCutoff) {
		// More than 4 minutes
		if(playerPosition > FOUR_MINUTES) {
			scrobble(context, song, true, isPastCutoff);
		}
		// More than 50% played
		else if(duration > 0 && playerPosition > (duration / 2)) {
			scrobble(context, song, true, isPastCutoff);
		}
	}

	public void scrobble(final Context context, final DownloadFile song, final boolean submission, final boolean isPastCutoff) {
		if(song == null) {
			return;
		}

		final String id = song.getSong().getId();
		// Avoid duplicate registrations.
		if (submission && id.equals(lastSubmission)) {
			return;
		}
		if (!submission && id.equals(lastNowPlaying)) {
			return;
		}

		if (submission) {
			lastSubmission = id;
		} else {
			lastNowPlaying = id;
		}

		new SilentBackgroundTask<Void>(context) {
			@Override
			protected Void doInBackground() {
				if(isPastCutoff) {
					SongDBHandler.getHandler(context).setSongPlayed(song, submission);
				}

				// Scrobbling disabled
				if (!Util.isScrobblingEnabled(context)) {
					return null;
				}
				// Ignore if online with no network access
				else if(!Util.isOffline(context) && !Util.isNetworkConnected(context)) {
					return null;
				}
				// Ignore podcasts
				else if(song.getSong() instanceof PodcastEpisode || song.getSong() instanceof InternetRadioStation) {
					return null;
				}

				// Ignore songs which are under 30 seconds per Last.FM guidelines
				Integer duration = song.getSong().getDuration();
				if(duration != null && duration > 0 && duration < 30) {
					return null;
				}

				MusicService service = MusicServiceFactory.getMusicService(context);
				try {
					service.scrobble(id, submission, context, null);
					Log.i(TAG, "Scrobbled '" + (submission ? "submission" : "now playing") + "' for " + song);
				} catch (Exception x) {
					Log.i(TAG, "Failed to scrobble'" + (submission ? "submission" : "now playing") + "' for " + song, x);
				}
				return null;
			}
		}.execute();
	}
}
