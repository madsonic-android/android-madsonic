/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service;

import org.madsonic.android.domain.Version;

/**
 * Thrown if the REST API version implemented by the server is too old.
 *
 * @author Sindre Mehus
 */
public class ServerTooOldException extends Exception {

    private final String text;
    private final Version serverVersion;
    private final Version requiredVersion;

    public ServerTooOldException(String text, Version serverVersion, Version requiredVersion) {
        this.text = text;
        this.serverVersion = serverVersion;
        this.requiredVersion = requiredVersion;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (text != null) {
            builder.append(text).append(" ");
        }
        builder.append("Server API version too old. ");
        builder.append("Requires server version ")
			.append(requiredVersion.toString())
			.append(", but it is version ")
			.append(serverVersion.toString())
			.append(".");
        return builder.toString();
    }
	
	@Override
	public String getMessage() {
		return this.toString();
	}
}
