/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.service.parser;

import android.content.Context;
import org.madsonic.android.R;
import org.madsonic.android.domain.Lyrics;
import org.madsonic.android.util.ProgressListener;
import org.xmlpull.v1.XmlPullParser;

import java.io.Reader;

/**
 * @author Sindre Mehus
 */
public class LyricsParser extends AbstractParser {

    public LyricsParser(Context context, int instance) {
		super(context, instance);
	}

    public Lyrics parse(Reader reader, ProgressListener progressListener) throws Exception {
        init(reader);

        Lyrics lyrics = null;
        int eventType;
        do {
            eventType = nextParseEvent();
            if (eventType == XmlPullParser.START_TAG) {
                String name = getElementName();
                if ("lyrics".equals(name)) {
                    lyrics = new Lyrics();
                    lyrics.setArtist(get("artist"));
                    lyrics.setTitle(get("title"));
                } else if ("error".equals(name)) {
                    handleError();
                }
            } else if (eventType == XmlPullParser.TEXT) {
                if (lyrics != null && lyrics.getText() == null) {
                    lyrics.setText(getText());
                }
            }
        } while (eventType != XmlPullParser.END_DOCUMENT);

        validate();
        return lyrics;
    }
}
