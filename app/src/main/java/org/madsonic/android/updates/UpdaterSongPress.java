/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.updates;

import android.content.Context;
import android.content.SharedPreferences;

import org.madsonic.android.util.Constants;
import org.madsonic.android.util.Util;

public class UpdaterSongPress extends Updater {
	public UpdaterSongPress() {
		super(521);
		TAG = this.getClass().getSimpleName();
	}

	@Override
	public void update(Context context) {
		SharedPreferences prefs = Util.getPreferences(context);
		boolean playNowAfter = prefs.getBoolean("playNowAfter", true);

		// Migrate the old preference so behavior stays the same
		if(playNowAfter == false) {
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(Constants.PREFERENCES_KEY_SONG_PRESS_ACTION, "single");
			editor.commit();
		}
	}
}
