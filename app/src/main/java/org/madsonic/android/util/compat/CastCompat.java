/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.util.compat;

import android.support.v7.media.MediaRouter;

import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;

import org.madsonic.android.service.ChromeCastController;
import org.madsonic.android.service.DownloadService;
import org.madsonic.android.service.RemoteController;
import org.madsonic.android.util.EnvironmentVariables;

public final class CastCompat {
	static {
		try {
			Class.forName("com.google.android.gms.cast.CastDevice");
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static void checkAvailable() throws Throwable {
		// Calling here forces class initialization.
	}

	public static RemoteController getController(DownloadService downloadService, MediaRouter.RouteInfo info) {
		CastDevice device = CastDevice.getFromBundle(info.getExtras());
		if(device != null) {
			return new ChromeCastController(downloadService, device);
		} else {
			return null;
		}
	}

	public static String getCastControlCategory() {
		return CastMediaControlIntent.categoryForCast(EnvironmentVariables.CAST_APPLICATION_ID);
	}
}
