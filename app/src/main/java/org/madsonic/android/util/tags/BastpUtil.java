/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Copyright 2013 (C) Adrian Ulrich
 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.util.tags;

import android.support.v4.util.LruCache;
import java.util.HashMap;
import java.util.Vector;

public final class BastpUtil {
	private static final RGLruCache rgCache = new RGLruCache(16);

	/** Returns the ReplayGain values of 'path' as <track,album>
	 */
	public static float[] getReplayGainValues(String path) {
		float[] cached = rgCache.get(path);

		if(cached == null) {
			cached = getReplayGainValuesFromFile(path);
			rgCache.put(path, cached);
		}
		return cached;
	}
	
	
	
	/** Parse given file and return track,album replay gain values
	 */
	private static float[] getReplayGainValuesFromFile(String path) {
		String[] keys = { "REPLAYGAIN_TRACK_GAIN", "REPLAYGAIN_ALBUM_GAIN" };
		float[] adjust= { 0f                     , 0f                      };
		HashMap tags  = (new Bastp()).getTags(path);
		
		for (int i=0; i<keys.length; i++) {
			String curKey = keys[i];
			if(tags.containsKey(curKey)) {
				String rg_raw = (String)((Vector)tags.get(curKey)).get(0);
				String rg_numonly = "";
				float rg_float = 0f;
				try {
					String nums = rg_raw.replaceAll("[^0-9.-]","");
					rg_float = Float.parseFloat(nums);
				} catch(Exception e) {}
				adjust[i] = rg_float;
			}
		}
		return adjust;
	}
	
	/** LRU cache for ReplayGain values
	 */
	private static class RGLruCache extends LruCache<String, float[]> {
		public RGLruCache(int size) {
			super(size);
		}
	}

}

