/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.util.Notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;

@TargetApi(26)
public class NotificationsUtils extends ContextWrapper {

   /**/     private NotificationManager mManager;

        public static final String APP_CHANNEL_ID = "org.madsonic.android.app";
        public static final String CHAT_CHANNEL_ID = "org.madsonic.android.chat";
        public static final String SYNC_CHANNEL_ID = "org.madsonic.android.sync";

        public static final String APP_CHANNEL_NAME = "APP CHANNEL";
        public static final String CHAT_CHANNEL_NAME = "CHAT CHANNEL";
        public static final String SYNC_CHANNEL_NAME = "SYNC CHANNEL";

        public NotificationsUtils(Context base) {
            super(base);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createChannels();
                String groupId = "group_id_101";
                CharSequence groupName = "Madsonic Group";
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.createNotificationChannelGroup(new NotificationChannelGroup(groupId, groupName));
            }
        }

        public void createChannels() {

            // create app channel
            NotificationChannel androidChannel = new NotificationChannel(APP_CHANNEL_ID, APP_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            androidChannel.enableLights(false);
            androidChannel.enableVibration(false);
            androidChannel.setLightColor(Color.GREEN);
            androidChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            getManager().createNotificationChannel(androidChannel);

            // create sync channel
            NotificationChannel syncChannel = new NotificationChannel(SYNC_CHANNEL_ID, SYNC_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            syncChannel.enableLights(false);
            syncChannel.enableVibration(false);
            syncChannel.setLightColor(Color.GREEN);
            syncChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            getManager().createNotificationChannel(syncChannel);


            // create chat channel
            NotificationChannel chatChannel = new NotificationChannel(CHAT_CHANNEL_ID, CHAT_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            chatChannel.enableLights(true);
            chatChannel.enableVibration(true);
            chatChannel.setLightColor(Color.GRAY);
            chatChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            getManager().createNotificationChannel(chatChannel);
        }

//        public Notification.Builder getAppChannelNotification(String title, String body) {
//            return new Notification.Builder(getApplicationContext(), APP_CHANNEL_ID)
//                    .setContentTitle(title)
//                    .setContentText(body)
//                    .setSmallIcon(android.R.drawable.stat_notify_more)
//                    .setAutoCancel(true);
//        }

//        public Notification.Builder getChatChannelNotification(String title, String body) {
//            return new Notification.Builder(getApplicationContext(), CHAT_CHANNEL_ID)
//                    .setContentTitle(title)
//                    .setContentText(body)
//                    .setSmallIcon(android.R.drawable.stat_notify_more)
//                    .setAutoCancel(true);
//        }

//        public static Notification.Builder getSyncChannelNotification(Context context, String title, String body) {
//            return new Notification.Builder(context, SYNC_CHANNEL_ID)
//                    .setContentTitle(title)
//                    .setContentText(body)
//                    .setSmallIcon(android.R.drawable.stat_notify_more)
//                    .setAutoCancel(true);
//        }

        public NotificationManager getManager() {
            if (mManager == null) {
                mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            }
            return mManager;
        }

        public void deleteNotificationChannel(String channelId) {
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.deleteNotificationChannel(channelId);
        }
    }