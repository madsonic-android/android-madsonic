package org.madsonic.android.util.buffer;

import android.content.Context;
import android.util.Log;

import org.madsonic.android.domain.MusicDirectory;
import org.madsonic.android.service.DownloadService;
import org.madsonic.android.service.MusicService;
import org.madsonic.android.service.MusicServiceFactory;
import org.madsonic.android.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FollowMePlayBuffer {

    private static final String TAG = FollowMePlayBuffer.class.getSimpleName();

    private static final int CAPACITY = 20;
    private static final int REFILL_THRESHOLD = 11;

    private final ScheduledExecutorService executorService;
    private final List<MusicDirectory.Entry> buffer = new ArrayList<MusicDirectory.Entry>();

    private int currentServer;
    private Runnable runnable;
    private DownloadService context;

    public FollowMePlayBuffer(DownloadService context) {
        this.context = context;
        executorService = Executors.newSingleThreadScheduledExecutor();

		runnable = new Runnable() {
            @Override
            public void run() {
                    refill();
            }
        };
       // executorService.scheduleWithFixedDelay(runnable, 1, 30, TimeUnit.SECONDS);
    }

    public List<MusicDirectory.Entry> get(int size) {
        clearBufferIfnecessary();

        List<MusicDirectory.Entry> result = new ArrayList<MusicDirectory.Entry>(size);
        synchronized (buffer) {
            while (!buffer.isEmpty() && result.size() < size) {
                result.add(buffer.remove(buffer.size() - 1));
            }
        }
        Log.i(TAG,"Taking " + result.size() + " songs from FollowMe play buffer. " + buffer.size() + " remaining.");
        return result;
    }

    public void shutdown() {
        executorService.shutdown();
    }

    public int size() {
        synchronized (buffer) {
            return buffer.size();
        }
    }

    public void clear() {
        synchronized (buffer) {
            buffer.clear();
        }
    }

    public void refill(int id) throws Throwable {

        // Check if active server has changed.
        clearBufferIfnecessary();

        if (buffer.size() > REFILL_THRESHOLD || (!Util.isNetworkConnected(context) && !Util.isOffline(context))) {
            return;
        }

        MusicService service = MusicServiceFactory.getMusicService(context);
        MusicDirectory songs = service.getFollowMeSongs(id, context, null);
     // TODO:XXX   CurrentPlayBuffer currentPlayBuffer = context.getCurrentPlayBuffer();
     // TODO:XXX   List<MusicDirectory.Entry> currentPlayed = currentPlayBuffer.getall();

        synchronized (buffer) {

            int resultList = CAPACITY - buffer.size();
            int count = 0;

            for (MusicDirectory.Entry newEntry : songs.getChildren() ) {
                if (buffer.size() < resultList) {

//                    if (currentPlayed.contains(newEntry)) {
//                        Log.w(TAG,"Removed duplicate buffer entry!");
//                    } else {
//                        buffer.add(newEntry);
//                        count++;
//                    }
                }
            }
            Log.i(TAG,"Refilled FollowMe play buffer with " + count + " songs. Based on id: " + id + " Buffersize: " + buffer.size());
        }

    }

    public void refill() {

        // Check if active server has changed.
        clearBufferIfnecessary();

        if (buffer.size() > REFILL_THRESHOLD || (!Util.isNetworkConnected(context) && !Util.isOffline(context))) {
            return;
        }

        try {
            MusicService service = MusicServiceFactory.getMusicService(context);
   //         DownloadService downloadService = context;
   //         CurrentPlayBuffer currentPlayBuffer = downloadService.getCurrentPlayBuffer();
   //         List<MusicDirectory.Entry> recentPlayed = currentPlayBuffer.getall();
            MusicDirectory.Entry lastPlayed = new MusicDirectory.Entry();
            MusicDirectory songs = new MusicDirectory();

  //          if (recentPlayed.size()>0) {
  //              lastPlayed = recentPlayed.get(recentPlayed.size()-1);

                lastPlayed.setId("2800");
                songs = service.getFollowMeSongs(Integer.valueOf(lastPlayed.getId()), context, null);

                if (songs.getChildren().size() > 0) {
                    Log.i(TAG,"Refilled buffer Candidates -> LASTPLAYED -> " + lastPlayed.getArtist() + " - " + lastPlayed.getTitle() + " -> " + songs.getChildren().size() );
                } else {
                    Log.i(TAG,"no files found");
                    return;
                }
    //        }


            synchronized (buffer) {

                int resultList = CAPACITY; // - buffer.size();
                int count = 0;

                for (MusicDirectory.Entry newEntry : songs.getChildren() ) {
                    if (buffer.size() < resultList) {

//                        if (recentPlayed.contains(newEntry)) {
//                            Log.i(TAG,"KILLED duplicate!");
//                        } else {
                            buffer.add(newEntry);
                            count++;
//                        }
                    }

                }

                if (count > 0) {
                    Log.i(TAG,"added to FollowMe buffer " + count + " songs. Buffersize now: " + buffer.size());
                }
            }
        } catch (Exception x) {
            Log.w(TAG,"Failed to refill FollowMe play buffer.", x);
        }
    }

    private void clearBufferIfnecessary() {
        synchronized (buffer) {
            if (currentServer != Util.getActiveServer(context)) {
                currentServer = Util.getActiveServer(context);
                buffer.clear();
            }
        }
    }

}
