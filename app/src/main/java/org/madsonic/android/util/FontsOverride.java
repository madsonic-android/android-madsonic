package org.madsonic.android.util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;

import org.madsonic.android.R;

import java.lang.reflect.Field;

/**
 * Created by admin on 14.09.2017.
 */

public final class FontsOverride {

    public static void setDefaultFont(Context context,String staticTypefaceFieldName, String fontAssetName) {
        final Typeface typeface = ResourcesCompat.getFont(context, R.font.dosis_400);
        replaceFont(staticTypefaceFieldName, typeface);
    }

    protected static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
