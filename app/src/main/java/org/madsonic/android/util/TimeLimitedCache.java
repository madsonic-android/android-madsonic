/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.util;

import java.lang.ref.SoftReference;
import java.util.concurrent.TimeUnit;

/**
 * @author Sindre Mehus
 */
public class TimeLimitedCache<T> {

    private SoftReference<T> value;
    private final long ttlMillis;
    private long expires;

    public TimeLimitedCache(long ttl, TimeUnit timeUnit) {
        this.ttlMillis = TimeUnit.MILLISECONDS.convert(ttl, timeUnit);
    }

    public T get() {
        return System.currentTimeMillis() < expires ? value.get() : null;
    }

    public void set(T value) {
        set(value, ttlMillis, TimeUnit.MILLISECONDS);
    }

    public void set(T value, long ttl, TimeUnit timeUnit) {
        this.value = new SoftReference<T>(value);
        expires = System.currentTimeMillis() + timeUnit.toMillis(ttl);
    }

    public void clear() {
        expires = 0L;
        value = null;
    }
}
