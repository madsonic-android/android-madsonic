package org.madsonic.android.util.buffer;

import android.content.Context;
import android.util.Log;

import org.madsonic.android.domain.MusicDirectory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CurrentPlayBuffer {

    private static final String TAG = CurrentPlayBuffer.class.getSimpleName();

    private static final int CAPACITY = 100;
    private static final int CLEAN_THRESHOLD = 51;

    private final List<MusicDirectory.Entry> buffer = new ArrayList<MusicDirectory.Entry>();

    private final ScheduledExecutorService executorService;

    private Context context;


    public CurrentPlayBuffer(Context context) {
        this.context = context;
        executorService = Executors.newSingleThreadScheduledExecutor();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                truncate();
            }
        };
        executorService.scheduleWithFixedDelay(runnable, 1, 60, TimeUnit.SECONDS);
    }

    public List<MusicDirectory.Entry> getall(){
        synchronized (buffer) {
            return buffer;
        }
    }


    private void truncate(){
        synchronized (buffer) {
            if (buffer.size() >= CLEAN_THRESHOLD) {
                buffer.remove(0);
                Log.i(TAG, "Truncate currentPlayBuffer: " + buffer.size());
            }
        }
    }


    public void add(MusicDirectory.Entry entry){

        synchronized (buffer) {
            if (!buffer.contains(entry)) {
                buffer.add(entry);
                Log.i(TAG, "Add ID " + entry.getId() + " to buffer. Buffersize: " + buffer.size());
            }
        }
    }
}

