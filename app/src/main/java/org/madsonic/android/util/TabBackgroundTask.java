/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.util;

import org.madsonic.android.domain.Node;
import org.madsonic.android.fragments.MadsonicFragment;

import java.util.List;

/**
 * @author Sindre Mehus
 */
public abstract class TabBackgroundTask<T> extends BackgroundTask<T> {

    private final MadsonicFragment tabFragment;

    public TabBackgroundTask(MadsonicFragment fragment) {
        super(fragment.getActivity());
        tabFragment = fragment;
    }

    @Override
    public void execute() {
        tabFragment.setProgressVisible(true);

		queue.offer(task = new Task() {
			@Override
			public void onDone(T result) {
				tabFragment.setProgressVisible(false);
				done(result);
			}

			@Override
			public void onError(Throwable t) {
				tabFragment.setProgressVisible(false);
				error(t);
			}
		});
    }

	@Override
    public boolean isCancelled() {
        return !tabFragment.isAdded() || cancelled.get();
    }

    @Override
    public void updateProgress(final String message) {
        getHandler().post(new Runnable() {
            @Override
            public void run() {
                tabFragment.updateProgress(message);
            }
        });
    }
}
