/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

import org.madsonic.android.activity.MadsonicActivity;

/**
 * @author Sindre Mehus
 */
public abstract class LoadingTask<T> extends BackgroundTask<T> {

    private final Activity tabActivity;
	private ProgressDialog loading;
	private final boolean cancellable;

	public LoadingTask(Activity activity) {
		super(activity);
		tabActivity = activity;
		this.cancellable = true;
	}
    public LoadingTask(Activity activity, final boolean cancellable) {
        super(activity);
        tabActivity = activity;
		this.cancellable = cancellable;
    }

    @Override
    public void execute() {
        loading = ProgressDialog.show(tabActivity, "", "Loading. Please Wait...", true, cancellable, new DialogInterface.OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				cancel();
			}
		});

		queue.offer(task = new Task() {
			@Override
			public void onDone(T result) {
				if(loading.isShowing()) {
					loading.dismiss();
				}
				done(result);
			}

			@Override
			public void onError(Throwable t) {
				if(loading.isShowing()) {
					loading.dismiss();
				}
				error(t);
			}
		});
    }

	@Override
    public boolean isCancelled() {
        return (tabActivity instanceof MadsonicActivity && ((MadsonicActivity) tabActivity).isDestroyedCompat()) || cancelled.get();
    }
	
	@Override
    public void updateProgress(final String message) {
		if(!cancelled.get()) {
			getHandler().post(new Runnable() {
				@Override
				public void run() {
						loading.setMessage(message);
				}
			});
		}
    }
}
