/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.domain;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

import org.madsonic.android.util.Constants;
import org.madsonic.android.util.Util;

public class Indexes implements Serializable {

    private long lastModified;
    private List<Artist> shortcuts;
    private List<Artist> artists;
	private List<MusicDirectory.Entry> entries;

	public Indexes() {

	}
    public Indexes(long lastModified, List<Artist> shortcuts, List<Artist> artists) {
        this.lastModified = lastModified;
        this.shortcuts = shortcuts;
        this.artists = artists;
		this.entries = new ArrayList<MusicDirectory.Entry>();
    }
	public Indexes(long lastModified, List<Artist> shortcuts, List<Artist> artists, List<MusicDirectory.Entry> entries) {
		this.lastModified = lastModified;
		this.shortcuts = shortcuts;
		this.artists = artists;
		this.entries = entries;
	}

    public long getLastModified() {
        return lastModified;
    }

    public List<Artist> getShortcuts() {
        return shortcuts;
    }

    public List<Artist> getArtists() {
        return artists;
    }

	public void setArtists(List<Artist> artists) {
		this.shortcuts = new ArrayList<Artist>();
		this.artists.clear();
		this.artists.addAll(artists);
	}

	public List<MusicDirectory.Entry> getEntries() {
		return entries;
	}

	public void sortChildren(Context context) {
		SharedPreferences prefs = Util.getPreferences(context);
		String ignoredArticlesString = prefs.getString(Constants.CACHE_KEY_IGNORE, "The El La Los Las Le Les");
		final String[] ignoredArticles = ignoredArticlesString.split(" ");

		Artist.sort(shortcuts, ignoredArticles);
		Artist.sort(artists, ignoredArticles);
	}
}