/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.domain;

import android.media.RemoteControlClient;

/**
 * @author Sindre Mehus
 * @version $Id$
 */
public enum PlayerState {
	IDLE(RemoteControlClient.PLAYSTATE_STOPPED),
    DOWNLOADING(RemoteControlClient.PLAYSTATE_BUFFERING),
    PREPARING(RemoteControlClient.PLAYSTATE_BUFFERING),
    PREPARED(RemoteControlClient.PLAYSTATE_STOPPED),
    STARTED(RemoteControlClient.PLAYSTATE_PLAYING),
    STOPPED(RemoteControlClient.PLAYSTATE_STOPPED),
    PAUSED(RemoteControlClient.PLAYSTATE_PAUSED),
	PAUSED_TEMP(RemoteControlClient.PLAYSTATE_PAUSED),
    COMPLETED(RemoteControlClient.PLAYSTATE_STOPPED);
    
    private final int mRemoteControlClientPlayState;
    
    private PlayerState(int playState) {
    	mRemoteControlClientPlayState = playState;
    }
    
    public int getRemoteControlClientPlayState() {
    	return mRemoteControlClientPlayState;
    }
}
