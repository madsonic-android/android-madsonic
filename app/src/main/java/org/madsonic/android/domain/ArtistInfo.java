/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.domain;

import java.io.Serializable;
import java.util.List;

public class ArtistInfo implements Serializable {

	private String biography;
	private String musicBrainzId;
	private String lastFMUrl;
	private String imageUrl;
	private List<Artist> similarArtists;
	private List<String> missingArtists;

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getMusicBrainzId() {
		return musicBrainzId;
	}

	public void setMusicBrainzId(String musicBrainzId) {
		this.musicBrainzId = musicBrainzId;
	}

	public String getLastFMUrl() {
		return lastFMUrl;
	}

	public void setLastFMUrl(String lastFMUrl) {
		this.lastFMUrl = lastFMUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<Artist> getSimilarArtists() {
		return similarArtists;
	}

	public void setSimilarArtists(List<Artist> similarArtists) {
		this.similarArtists = similarArtists;
	}

	public List<String> getMissingArtists() {
		return missingArtists;
	}

	public void setMissingArtists(List<String> missingArtists) {
		this.missingArtists = missingArtists;
	}
}
