/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.domain;

import java.io.Serializable;

/**
 * Represents the version number of the Madsonic Android app.
 */
public class Version implements Comparable<Version>, Serializable {
	private int major;
	private int minor;
	private int build;

	public Version() {
		// For Kryo
	}

	/**
	 * Creates a new version instance by parsing the given string.
	 * @param version A string of the format "1.15", "2.0.5" or "3.0.0".
	 */
	public Version(String version) {
		String[] s = version.split("\\.");
		major = Integer.valueOf(s[0]);
		minor = Integer.valueOf(s[1]);
        if (s.length > 2) {
            build = Integer.valueOf(s[2]);
        }
	}

	public int getMajor() {
		return major;
	}

	public int getMinor() {
		return minor;
	}

    public int getBuild() {
        return build;
    }

	public String getVersion() {
		switch(major) {
			case 1:
				switch(minor) {
					case 14:
						return "6.0";
					case 15:
						return "6.1";
				}
				case 2: return "6.2";

				case 3: return "6.3";
				}
		return "";
	}

	/**
	 * Return whether this object is equal to another.
	 * @param o Object to compare to.
	 * @return Whether this object is equals to another.
	 */
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final Version version = (Version) o;

		if (build != version.build) return false;
		if (major != version.major) return false;
		return minor == version.minor;
	}

	/**
	 * Returns a hash code for this object.
	 * @return A hash code for this object.
	 */
	public int hashCode() {
		int result;
		result = major;
		result = 29 * result + minor;
		result = 29 * result + build;
		return result;
	}

	/**
	 * Returns a string representation of the form "1.27", "1.27.2" or "1.27.beta3".
	 * @return A string representation of the form "1.27", "1.27.2" or "1.27.beta3".
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(major).append('.').append(minor);
		if (build != 0) {
            buf.append(".").append(build);
        }
		return buf.toString();
	}

	/**
	 * Compares this object with the specified object for order.
	 * @param version The object to compare to.
	 * @return A negative integer, zero, or a positive integer as this object is less than, equal to, or
	 * greater than the specified object.
	 */
	@Override
	public int compareTo(Version version) {
		if (major < version.major) {
			return -1;
		} else if (major > version.major) {
			return 1;
		}

		if (minor < version.minor) {
			return -1;
		} else if (minor > version.minor) {
			return 1;
		}

		if (build < version.build) {
			return -1;
		} else if (build > version.build) {
			return 1;
		}

		return 0;
	}
}