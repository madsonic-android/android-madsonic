/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.domain;

import java.io.Serializable;
import java.util.List;

/**
 * The result of a search.  Contains matching artists, albums and songs.
 */
public class SearchResult implements Serializable {

    private final List<Artist> artists;
    private final List<MusicDirectory.Entry> albums;
    private final List<MusicDirectory.Entry> songs;

    public SearchResult(List<Artist> artists, List<MusicDirectory.Entry> albums, List<MusicDirectory.Entry> songs) {
        this.artists = artists;
        this.albums = albums;
        this.songs = songs;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public List<MusicDirectory.Entry> getAlbums() {
        return albums;
    }

    public List<MusicDirectory.Entry> getSongs() {
        return songs;
    }

	public boolean hasArtists() {
		return !artists.isEmpty();
	}
	public boolean hasAlbums() {
		return !albums.isEmpty();
	}
	public boolean hasSongs() {
		return !songs.isEmpty();
	}
}