/*
 This file is part of Madsonic.

 Madsonic is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Madsonic is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Madsonic.  If not, see <http://www.gnu.org/licenses/>.

 Based upon Dsub, Copyright 2013-2017 (C) Scott Jackson
 Based upon Subsonic, Copyright 2004-2017 (C) Sindre Mehus
 Based upon Madsonic, Copyright 2016-2017 (C) Martin Karel
  */

package org.madsonic.android.domain;

import android.util.Log;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a top level directory in which music or other media is stored.
 *
 * @author Sindre Mehus, Martin Karel
 */
public class MusicFolder implements Serializable {

	private static final String TAG = MusicFolder.class.getSimpleName();
	private String id;
	private String name;
	private int type;
	private int group;
	private boolean enabled;

	public MusicFolder() {

	}
	public MusicFolder(String id, String name, int type, int group) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.group = group;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean getEnabled() {
		return enabled;
	}

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getGroup() {
        return group;
    }

    public static class MusicFolderComparator implements Comparator<MusicFolder> {
		public int compare(MusicFolder lhsMusicFolder, MusicFolder rhsMusicFolder) {
			if(lhsMusicFolder == rhsMusicFolder || lhsMusicFolder.getName().equals(rhsMusicFolder.getName())) {
				return 0;
			} else {
				return lhsMusicFolder.getName().compareToIgnoreCase(rhsMusicFolder.getName());
			}
		}
	}

	public static void sort(List<MusicFolder> musicFolders) {
		try {
			Collections.sort(musicFolders, new MusicFolderComparator());
		} catch (Exception e) {
			Log.w(TAG, "Failed to sort music folders", e);
		}
	}
}
